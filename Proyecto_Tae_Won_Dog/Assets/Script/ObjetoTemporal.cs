﻿using UnityEngine;
using System.Collections;

public class ObjetoTemporal : MonoBehaviour {
	public float tiempoEnVida = 1f;
	public bool soloDesactivar = false;
	// Use this for initialization
	void Start () {
		if (!soloDesactivar){
			Destroy (gameObject, tiempoEnVida);
		}
		else{
			Invoke("DesactivarObjeto",tiempoEnVida);
		}

	}
	void DesactivarObjeto(){
		gameObject.SetActive (false);
	}

}
