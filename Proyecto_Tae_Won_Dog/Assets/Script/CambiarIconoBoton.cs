﻿using UnityEngine;
using System.Collections;

public class CambiarIconoBoton : MonoBehaviour {
	public Texture iconoOn;
	public Texture iconoOff;

	void OnMouseDown() {
		GetComponent<Renderer>().material.mainTexture = iconoOff;
		Invoke ("VolverAIconoOn",0.15f);
	}

	private void VolverAIconoOn(){
		GetComponent<Renderer>().material.mainTexture = iconoOn;
	}
}
