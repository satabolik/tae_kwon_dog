﻿using UnityEngine;
using System.Collections;

public class ControladorSostenedoresTablon : MonoBehaviour {
	public GameObject banca;
	[Range(0,2)]
	public int numeroDeHijosActivos = 0;
	private const float EJE_Y_BANCA = -2.49983f;

	void Start () {
		ActivarHijos ();
		Vector3 posicionBanca = new Vector3 (transform.position.x,EJE_Y_BANCA,transform.position.z);
		banca = Instantiate (banca, posicionBanca, Quaternion.identity) as GameObject;
		banca.transform.parent = transform;
	}
	void ActivarHijos(){
		for(int i=0;i<=numeroDeHijosActivos;i++){
			transform.GetChild (i).gameObject.SetActive(true);
		}
	}

}
