﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingHandler : PopupHandler
{
    [SerializeField]
    private Button btnSound;
    [SerializeField]
    private Button btnMusic;
    [SerializeField]
    private Button btnRetry;
    [SerializeField]
    private Button btnHome;
    [SerializeField]
    private Image imgBlockedSound;
    [SerializeField]
    private Image imgBlockedMusic;
    [SerializeField]
    private ToggleFlagLenguage[] togglesFlags;

    // Start is called before the first frame update
    void Start()
    {
        if (btnHome != null)
            btnHome.onClick.AddListener(OnClickHome);

        if (btnRetry != null)
            btnRetry.onClick.AddListener(OnClickRetry);

        if (btnMusic != null)
            btnMusic.onClick.AddListener(OnClickBtnMusic);

        if (btnSound != null)
            btnSound.onClick.AddListener(OnClickBtnSound);

        if (togglesFlags != null)
        {
            var currentLocale = LocalizationManager.Instance.CurrentLocale;
            foreach (var tgf in togglesFlags)
            {
                tgf.tgFlag.isOn = (tgf.type == currentLocale);
                tgf.tgFlag.onValueChanged.AddListener((isOn) =>
                {
                    if (isOn)
                        OnSelectFlagToggle(tgf.type);
                });
            }
        }

        imgBlockedSound.enabled = !SaveDataManager.Instance.IsOnSound;
        imgBlockedMusic.enabled = !SaveDataManager.Instance.IsOnMusic;
    }

    protected override GameObject GetInitialFocusInteractable()
    {
        return btnSound.gameObject;
    }

    protected override void OnCancel()
    {
        base.OnCancel();
        ShowPopUp(false);
    }

    private void OnSelectFlagToggle(LocaleTypes type)
    {
        LocalizationManager.Instance.ChangeLocale(type);
    }

    public bool IsVisible()
    {
        return canvasBase.enabled;
    }

    private void OnClickBtnSound()
    {
        var isOn = SaveDataManager.Instance.IsOnSound;
        SaveDataManager.Instance.SaveSoundSetting(!isOn);
        SoundManager.Instance.PlaySfx(SfxType.PressButton);
        imgBlockedSound.enabled = !SaveDataManager.Instance.IsOnSound;
    }

    private void OnClickBtnMusic()
    {
        var isOn = SaveDataManager.Instance.IsOnMusic;
        SaveDataManager.Instance.SaveMusicSetting(!isOn);
        SoundManager.Instance.PlayMusic(!isOn);
        SoundManager.Instance.PlaySfx(SfxType.PressButton);
        imgBlockedMusic.enabled = !SaveDataManager.Instance.IsOnMusic;
    }

    private void OnClickRetry()
    {
        btnRetry.interactable = false;
        GameplayManager.Instance.ScaleTimeCtrll.ResetSpeed();
        SoundManager.Instance.PlaySfx(SfxType.PressButton, () => SceneManager.LoadScene(ScenesNames.GAMEPLAY));
    }

    private void OnClickHome()
    {
        btnHome.interactable = false;
        GameplayManager.Instance.ScaleTimeCtrll.ResetSpeed();
        SoundManager.Instance.PlaySfx(SfxType.PressButton, () => SceneManager.LoadScene(ScenesNames.MAIN));
    }

    [System.Serializable]
    public struct ToggleFlagLenguage
    {
        public LocaleTypes type;
        public Toggle tgFlag;
    }
}
