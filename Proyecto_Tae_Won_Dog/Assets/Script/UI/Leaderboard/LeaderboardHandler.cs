﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LeaderboardHandler : PopupHandler
{
    [SerializeField]
    private LeaderboardPlayerRecord playerRecordTemplate;
    [SerializeField]
    private List<LeaderboardRecordColor> playerRecordColors;
    [SerializeField]
    private int InitialPlayerRecordsInstantiated = 20;

    private Dictionary<int, LeaderboardPlayerRecord> dicLeaderboardplayerRecordsSlots;
    private Dictionary<int, Color> dicLeaderboardColorByRank;

    private static LeaderboardHandler instance;

    protected override void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        base.Awake();
        playerRecordTemplate.gameObject.SetActive(false);
        dicLeaderboardplayerRecordsSlots = new Dictionary<int, LeaderboardPlayerRecord>();
        for (int i = 1; i < InitialPlayerRecordsInstantiated; i++)
        {
            GetPlayerRecordSlot(i);
        }

        dicLeaderboardColorByRank = new Dictionary<int, Color>();
        foreach (var recordColor in playerRecordColors)
        {
            dicLeaderboardColorByRank.Add(recordColor.rank, recordColor.color);
        }
    }

    protected override void OnCancel()
    {
        base.OnCancel();
        ShowPopUp(false);
    }

    public void UpdateLeaderboardData(List<LeaderboardPlayerRecordData> leaderboardData)
    {
        foreach(var player in leaderboardData.OrderBy((d)=>d.playerRank))
        {
            var recordGO = GetPlayerRecordSlot(player.playerRank);
            dicLeaderboardColorByRank.TryGetValue(player.playerRank, out var color);
            recordGO.SetData(player, color);
            recordGO.gameObject.SetActive(true);
        }
    }

    private LeaderboardPlayerRecord GetPlayerRecordSlot(int playerRank)
    {
        LeaderboardPlayerRecord slot;
        if (!dicLeaderboardplayerRecordsSlots.TryGetValue(playerRank, out slot))
        {
            slot = Instantiate(playerRecordTemplate, playerRecordTemplate.transform.parent);
            slot.gameObject.name = $"slot_rank_{playerRank}";
            slot.transform.SetSiblingIndex(playerRank);
            dicLeaderboardplayerRecordsSlots.Add(playerRank, slot);
        }
        return slot;
    }
}

public struct LeaderboardPlayerRecordData
{
    public string playerName;
    public int playerScore;
    public int playerRank;
    public Texture2D playerAvatar;
    public bool isLocalUser;
}

[System.Serializable]
public struct LeaderboardRecordColor
{
    public Color color;
    public int rank;
}