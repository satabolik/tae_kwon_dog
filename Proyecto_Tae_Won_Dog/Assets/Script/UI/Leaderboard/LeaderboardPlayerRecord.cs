﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LeaderboardPlayerRecord : MonoBehaviour
{
    [SerializeField]
    private TMP_Text txtRank;
    [SerializeField]
    private RawImage playerAvatar;
    [SerializeField]
    private TMP_Text txtPlayerNick;
    [SerializeField]
    private TMP_Text txtPlayerScore;
    [SerializeField]
    private Image imgBanner;

    public void SetData(LeaderboardPlayerRecordData data, Color color = default)
    {
        txtRank.text = $"{data.playerRank} :";
        playerAvatar.texture = data.playerAvatar;
        txtPlayerScore.text = data.playerScore.ToString();
        txtPlayerNick.text = data.playerName;
        imgBanner.enabled = color != default;
        imgBanner.color = color;
        txtPlayerNick.color = data.isLocalUser ? Color.yellow : Color.white;
    }
}
