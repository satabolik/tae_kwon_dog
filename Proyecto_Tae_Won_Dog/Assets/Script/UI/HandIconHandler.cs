﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandIconHandler : MonoBehaviour
{
    [SerializeField]
    private CanvasGroup canvasGp;
    private Animator anim;

    private const string ANIM_TG_TOUCH = "TouchTrigger";
    private const string ANIM_TG_HOLD = "HoldTrigger";
    private const string ANIM_TG_RESET = "ResetTrigger";

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        canvasGp.alpha = 0f;
    }

    public void ResetAnim()
    {
        canvasGp.alpha = 0f;
        anim.SetTrigger(ANIM_TG_RESET);
    }
    public void PlayTouchAnim()
    {
        canvasGp.alpha = 1f;
        anim.SetTrigger(ANIM_TG_TOUCH);
    }
    public void PlayHoldAnim()
    {
        canvasGp.alpha = 1f;
        anim.SetTrigger(ANIM_TG_HOLD);
    }
}
