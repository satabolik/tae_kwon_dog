﻿using System;
using Unity.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PopupHandler : MonoBehaviour
{
    [SerializeField]
    private LayoutKeyEnum buttonLayoutKey;
    [SerializeField]
    protected LeanTweenType tweenCanvas;
    [SerializeField]
    protected float timeAnim;
    [SerializeField]
    protected Button btnAccept;
    [SerializeField]
    protected Button[] btnCancel;
    [SerializeField]
    protected CanvasGroup cgPanelPopup;
    [SerializeField]
    protected ScrollRect viewScrollRect;
    [SerializeField]
    private float gamepadScrollVelocity = 0.25f;

    protected int tweenAnimCanvasID = -1;
    protected GameObject previousSelectedGameObject;
    protected Canvas canvasBase;

    private static bool isAlreadyActivePopup = false;
    protected static PopupHandler previousPopupShow = null;


    public event Action OnAcceptEvent;
    public event Action OnCancelEvent;
    public event Action OnCompletedHideTrigger;


    protected virtual void Awake()
    {
        cgPanelPopup.alpha = 0f;
        cgPanelPopup.interactable = false;
        cgPanelPopup.blocksRaycasts = false;
        canvasBase = GetComponent<Canvas>();
        if (canvasBase) canvasBase.enabled = false;
    }

    private void ChangeButtonsInteraction(bool Interactables)
    {
        if (btnAccept != null)
        {
            btnAccept.interactable = Interactables;
        }
        foreach (var btn in btnCancel)
        {
            if (btn == null) continue;
            btn.interactable = Interactables;
        }
    }

    protected virtual void OnEnable()
    {
        if (btnAccept != null) 
        {
            btnAccept.onClick.AddListener(OnAccept);
        }
        foreach (var btn in btnCancel)
            btn.onClick.AddListener(OnCancel);
    }

    protected virtual void OnDisable()
    {
        if (btnAccept != null)
        {
            btnAccept.onClick.RemoveListener(OnAccept);
        }
        foreach (var btn in btnCancel)
            btn.onClick.RemoveListener(OnCancel);

        if (LeanTween.isTweening(tweenAnimCanvasID))
            LeanTween.cancel(tweenAnimCanvasID);

        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.OnUICancel -= OnCancel;
        }

        isAlreadyActivePopup = false;
    }

    protected virtual void OnAccept()
    {
        if (OnAcceptEvent != null)
            OnAcceptEvent();
    }
    protected virtual void OnCancel()
    {
        if (OnCancelEvent != null)
            OnCancelEvent();
    }

    public void ClearEvent()
    {
        OnAcceptEvent = null;
        OnCancelEvent = null;
    }

    public bool ShowPopUp(bool enable)
    {
        if (tweenAnimCanvasID != -1 && LeanTween.isTweening(tweenAnimCanvasID))
        {
            Debug.Log($"Cancel to show->[{enable}] to popup because is in animation");
            return false;
        }

        Action onAnimCompleted;
        if (enable)
        {
            if (isAlreadyActivePopup && !IgnoreAlreadyPopup()) { return false; };

            previousPopupShow = this;
            OnShowStart();
            onAnimCompleted = OnShowAnimatedEnd;
            if (canvasBase) canvasBase.enabled = true;
        }
        else
        {
            OnHideStart();
            onAnimCompleted = OnHideAnimatedEnd;      
        }

        cgPanelPopup.interactable = enable;
        cgPanelPopup.blocksRaycasts = enable;
        var start = (enable) ? 0f : 1f;
        var end = (!enable) ? 0f : 1f;
        tweenAnimCanvasID = LeanTween.value(start, end, timeAnim)
            .setIgnoreTimeScale(true)
            .setEase(tweenCanvas)
            .setOnUpdate(OnUpdateValueAnim)
            .setOnComplete(onAnimCompleted)
            .uniqueId;

        return true;
    }
    protected virtual void OnShowStart()
    {
        ChangeButtonsInteraction(false);
        ButtonLayoutManager.Instance?.ActiveButtonLayoutPanel(buttonLayoutKey);
        isAlreadyActivePopup = true;

        previousSelectedGameObject = EventSystem.current.currentSelectedGameObject;
        EventSystem.current.SetSelectedGameObject(GetInitialFocusInteractable());

        if (viewScrollRect != null)
        {
            LeanTween.value(0f, 1f, timeAnim * 0.5f).setOnUpdate((float v) =>
            {
                viewScrollRect.verticalNormalizedPosition = v;
            });
        }
    }
    protected virtual void OnShowAnimatedEnd()
    {
        ChangeButtonsInteraction(true);
        SettingConfigManager.Instance.OnUICancel += OnCancel;
        tweenAnimCanvasID = -1;

        if (viewScrollRect != null)
        {
            SettingConfigManager.Instance.OnUIGamepadScroll += Instance_OnUIGamepadScroll;
        }
    }

    protected virtual void OnHideStart()
    {
        SettingConfigManager.Instance.OnUICancel -= OnCancel;
        EventSystem.current.SetSelectedGameObject(previousSelectedGameObject);

        if (viewScrollRect != null)
        {
            SettingConfigManager.Instance.OnUIGamepadScroll -= Instance_OnUIGamepadScroll;
        }
    }

    protected void Instance_OnUIGamepadScroll(Vector2 vector2)
    {
        float normalizedPos = viewScrollRect.verticalNormalizedPosition + (gamepadScrollVelocity * vector2.y);
        viewScrollRect.verticalNormalizedPosition = Mathf.Clamp(normalizedPos, 0f, 1f);
    }

    protected virtual void OnHideAnimatedEnd()
    {
        StopAllCoroutines();
        if (canvasBase) canvasBase.enabled = false;
        isAlreadyActivePopup = false;
        ButtonLayoutManager.Instance?.ActivePreviousLayoutPanel();
        previousPopupShow = null;

        OnCompletedHideTrigger?.Invoke();
        OnCompletedHideTrigger = null;
    }

    protected virtual void OnUpdateValueAnim(float value)
    {
        if (cgPanelPopup == null)
            return;
        cgPanelPopup.transform.localScale = Vector3.one * value;
        cgPanelPopup.alpha = value;
    }

    protected virtual GameObject GetInitialFocusInteractable()
    {
        if (btnAccept != null)
        {
            return btnAccept.gameObject;
        }

        return gameObject;
    }

    protected virtual bool IgnoreAlreadyPopup()
    {
        return false;
    }
}
