﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TrophyDetail : MonoBehaviour
{
    [SerializeField]
    private RawImage imgTrophyIcon;
    [SerializeField]
    private TMP_Text txtTrophyName;
    [SerializeField]
    private TMP_Text txtTrophyDescription;
    [SerializeField]
    private Image imgBannerCompleted;

    public void SetData(TrophyDetailData data)
    {
        imgTrophyIcon.texture = data.icon;
        txtTrophyName.text = data.name;
        txtTrophyDescription.text = data.description;
        imgBannerCompleted.enabled = data.isAchieved;
    }

}
