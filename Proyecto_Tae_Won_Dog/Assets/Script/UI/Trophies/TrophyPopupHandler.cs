﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyPopupHandler : PopupHandler
{
    [SerializeField]
    private TrophyDetail TrophyDetailTempate;
    [SerializeField]
    private CanvasGroup cgContent;
    [SerializeField]
    private Image imgLoadingTropies;

    private Dictionary<string, TrophyDetail> dicTrophyDetailSlots;
    private TrophyPopupHandler instance;

    protected override void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        base.Awake();

        TrophyDetailTempate.gameObject.SetActive(false);
    }

    public void InitializeTrophySlots(List<string> achievementIDs)
    {
        if (dicTrophyDetailSlots == null)
        {
            dicTrophyDetailSlots = new Dictionary<string, TrophyDetail>();
        }

        foreach (string achievementID in achievementIDs)
        {
            if (dicTrophyDetailSlots.ContainsKey(achievementID)) { continue; }
            
            var trophySlot = Instantiate(TrophyDetailTempate, TrophyDetailTempate.transform.parent);
            trophySlot.gameObject.SetActive(true);
            trophySlot.gameObject.name = $"trophy_{achievementID}";
            dicTrophyDetailSlots.Add(achievementID, trophySlot);
        }
    }

    protected override void OnShowStart()
    {
        base.OnShowStart();
        StartCoroutine(AnimLoadingImageRutine());
        cgContent.alpha = 0f;
    }


    private IEnumerator AnimLoadingImageRutine()
    {
        imgLoadingTropies.enabled = true;
        while (true)
        {
            imgLoadingTropies.transform.Rotate(0, 0, -180 * Time.deltaTime);
            yield return null;
        }
    }

    private void StopAnimLoading()
    {
        imgLoadingTropies.enabled = false;
        StopAllCoroutines();
    }

    public void UpdateTrophiesDataList(List<TrophyDetailData> trophiesData)
    {
        StopAnimLoading();
        foreach (var trophyData in trophiesData)
        {
            if (dicTrophyDetailSlots.TryGetValue(trophyData.id, out var trophySlot))
            {
                trophySlot.SetData(trophyData);
            }
        }
        cgContent.alpha = 1f;

    }

    protected override void OnCancel()
    {
        base.OnCancel();
        ShowPopUp(false);
        StopAnimLoading();
    }
}

public struct TrophyDetailData
{
    public string id;
    public string name;
    public string description;
    public Texture2D icon;
    public float progress;
    public bool isAchieved;
}