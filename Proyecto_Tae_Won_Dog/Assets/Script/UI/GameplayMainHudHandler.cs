﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
public class GameplayMainHudHandler : MonoBehaviour
{
    [SerializeField]
    private TouchGameplayHandler gameplayHandler;
    [SerializeField]
    private CanvasGroup lifesCanvasGroup;
    [SerializeField]
    private LifePointHudHandler lifePointPrefab;
    [SerializeField]
    private PowerUpHandler powerUpHandler;
    [SerializeField]
    private Button btnSetting;
    [SerializeField]
    private SettingHandler pnlSetting;
    [SerializeField]
    private float scoreUpdateTime = 1f;
    [SerializeField]
    private LeanTweenType tweenScoreType;
    [SerializeField]
    private TMP_Text txtCurrentScore;
    [SerializeField]
    private TMP_Text txtHitScore;
    [SerializeField]
    private TMP_Text txtAddScore;
    [SerializeField]
    private Animator animHitScore;
    [SerializeField]
    private Animator animAddScore;
    [SerializeField]
    private AnimationEvent animEvent;

    private const string ANIM_TRIGGER_HIT = "hit";
    private const string ANIM_TRIGGER_FADEOUT = "desvanecer";
    private const string ANIM_TRIGGER_SHOW_ADD_SCORE = "ShowAddScore";
    private readonly string[] INPUT_SETTING = { "ShowAddScore", ""};


    private int currentPlusScore;
    private event Action OnEndAddScoreAnim;
    private bool isAnimAddScoreRun;
    private Stack<LifePointHudHandler> playerLifePointStack = new Stack<LifePointHudHandler>();

    private float lastX;

    void Start()
    {
        lifePointPrefab.gameObject.SetActive(false);
        UpdateScore(0, 0);
        CreateStartLife();
        btnSetting.onClick.AddListener(OnSetting);
        powerUpHandler.Set(GameplayManager.Instance.SpecialModeTime);
        txtHitScore.text = string.Empty;
        SettingConfigManager.Instance.SetInputMode(InputMode.Gameplay);
        ButtonLayoutManager.Instance.ActiveButtonLayoutPanel(LayoutKeyEnum.Gameplay);

    }

    private void OnSetting()
    {
        if (!pnlSetting.ShowPopUp(true))
        {
            return;
        }

        SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        SoundManager.Instance.PlaySfx(SfxType.PressButton);
        if (GameplayManager.Instance != null)
        {
            GameplayManager.Instance.ScaleTimeCtrll.StopTimeScale();
        }
    }


    private void PnlSetting_OnCancelEvent()
    {
        SettingConfigManager.Instance.SetInputMode(InputMode.Gameplay);
        if (GameplayManager.Instance != null)
        {
            GameplayManager.Instance.ScaleTimeCtrll.ResumeCurrentTimeScale();
        }
    }


    public void ViewScore(bool enable)
    {
        txtCurrentScore.enabled = enable;
    }
    public void ViewComboHit(bool enable)
    {
        txtHitScore.enabled = enable;
    }
    public void ViewLife(bool enable)
    {
        lifesCanvasGroup.alpha = (enable) ? 1f : 0f;
    }
    public void ViewPowerUp(bool enable)
    {
        powerUpHandler.ViewPowerUp(enable);
    }

    private void CreateStartLife()
    {
        var life = GameplayManager.Instance.PlayerLifePoint;
        for (int i = 0; i < life; i++)
        {
            AddLifeHud();
        }
    }

    public LifePointHudHandler AddLifeHud()
    {
        var newLifePto = Instantiate(lifePointPrefab, lifePointPrefab.transform.parent);
        newLifePto.name = $"life_Pto_{playerLifePointStack.Count}";
        newLifePto.gameObject.SetActive(true);
        playerLifePointStack.Push(newLifePto);
        return newLifePto;
    }


    private void OnEnable()
    {
        pnlSetting.OnCancelEvent += PnlSetting_OnCancelEvent;

        SettingConfigManager.Instance.OnGameplayOpenSetting += OnSetting;

        GlobaGameplaylEvents.OnUpdateScore += UpdateScore;
        GlobaGameplaylEvents.OnUpdateHitCount += OnUpdateHitCounter;
        GlobaGameplaylEvents.OnUpdateSpecial += EstadoJuego_OnUpdateEspecial;
        GlobaGameplaylEvents.OnUpdateLifePlayer += OnUpdatePlayerLife;
        if (GameplayManager.Instance != null)
        {
            powerUpHandler.OnClickEvt += GameplayManager.Instance.StartSpecialMode;

        }
        animEvent.OnAnimationEnd += OnEndAnimEnd;

    }
    private void OnDisable()
    {
        pnlSetting.OnCancelEvent -= PnlSetting_OnCancelEvent;

        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnGameplayOpenSetting -= OnSetting;

        GlobaGameplaylEvents.OnUpdateScore -= UpdateScore;
        GlobaGameplaylEvents.OnUpdateHitCount -= OnUpdateHitCounter;
        GlobaGameplaylEvents.OnUpdateSpecial -= EstadoJuego_OnUpdateEspecial;
        GlobaGameplaylEvents.OnUpdateLifePlayer -= OnUpdatePlayerLife;
        if (GameplayManager.Instance != null)
        {
            powerUpHandler.OnClickEvt -= GameplayManager.Instance.StartSpecialMode;

        }
        animEvent.OnAnimationEnd -= OnEndAnimEnd;

    }

    private void OnUpdatePlayerLife(bool addLife)
    {
        if (addLife)
        {
            AddLifeHud();
        }
        else
        {
            if (playerLifePointStack.Count == 0)
                return;
            var lifeLeft = playerLifePointStack.Pop();
            lifeLeft.TriggerLoss();
        }
    }

    void OnUpdateHitCounter(int hit)
    {
        if (hit > 0)
        {
            txtHitScore.text = $"x {hit}";
            animHitScore.SetTrigger(ANIM_TRIGGER_HIT);
        }
        else
        {
            if (currentPlusScore > 0)
                FinishAddScore();
            animHitScore.SetTrigger(ANIM_TRIGGER_FADEOUT);
        }


    }
    private void OnEndAnimEnd()
    {
        if (OnEndAddScoreAnim != null)
        {
            OnEndAddScoreAnim();
            OnEndAddScoreAnim = null;
        }
        isAnimAddScoreRun = false;
    }

    private void AnimAddScore(int valueAdd)
    {
        if (valueAdd == 0)
            return;
        if (isAnimAddScoreRun)
        {
            OnEndAddScoreAnim += () => AnimAddScore(valueAdd);
            return;
        }
        txtAddScore.text = $"+{valueAdd}";
        animAddScore.SetTrigger(ANIM_TRIGGER_SHOW_ADD_SCORE);
        isAnimAddScoreRun = true;
    }

    private void UpdateScore(int valueAdd, int totalScore)
    {
        currentPlusScore += valueAdd;
    }


    private void FinishAddScore()
    {
        int totalScore = GameplayManager.Instance.CurrentPoint;
        int current = 0;
        AnimAddScore(currentPlusScore);
        int.TryParse(txtCurrentScore.text, out current);
        //txtCurrentScore.text = totalScore.ToString();
        var tween = LeanTween.value(current, totalScore, scoreUpdateTime)
              .setEase(tweenScoreType)
              .setOnUpdate((float v) => txtCurrentScore.text = ((int)v).ToString())
              .setOnComplete(() => txtCurrentScore.text = totalScore.ToString());
        currentPlusScore = 0;
    }

    private void EstadoJuego_OnUpdateEspecial(int act, int max)
    {
        powerUpHandler.UpdateValue(act, max);

    }
}
