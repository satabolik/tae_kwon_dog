﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePointHudHandler : MonoBehaviour
{
    [SerializeField]
    private Animator animator;
    private const string LIFE_POINT_LOSS = "LifePointLoss";
    private bool isRunLoss;

    public void TriggerLoss()
    {
        if (isRunLoss)
            return;
        animator.SetTrigger(LIFE_POINT_LOSS);
    }
   private void OnEndLifeLossAnim()
    {
        this.gameObject.SetActive(false);
    }
}
