﻿using Steamworks;
using System;
using System.Collections;
using UnityEngine;

public class GamepadDisconnectPopup : PopupHandler
{

    protected override void OnEnable()
    {
        base.OnEnable();
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.OnActiveDeviceDisconnected += Instance_OnActiveDeviceDisconnected;
        }
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.OnActiveDeviceDisconnected -= Instance_OnActiveDeviceDisconnected;
        }
    }

    protected override void OnAccept()
    {
        ShowPopUp(false);
    }

    private void Instance_OnActiveDeviceDisconnected(DeviceType obj)
    {
        if (previousPopupShow != null)
        {
            previousPopupShow.OnCompletedHideTrigger += ()=> ShowPopUp(true);
            previousPopupShow.ShowPopUp(false);
            return;
        }

        ShowPopUp(true);
    }


    protected override bool IgnoreAlreadyPopup()
    {
        return true;
    }

    protected override void OnShowStart()
    {
        base.OnShowStart();
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        }

        if (GameplayManager.Instance != null)
        {
            GameplayManager.Instance.ScaleTimeCtrll.StopTimeScale();
        }
    }

    protected override void OnHideAnimatedEnd()
    {
        base.OnHideAnimatedEnd();

        if (SettingConfigManager.Instance != null)
        {
            if (GameplayManager.Instance != null && !GameplayManager.Instance.IsGameOver)
            {
                SettingConfigManager.Instance.SetInputMode(InputMode.Gameplay);
            }
        }
        if (GameplayManager.Instance != null)
        {
            GameplayManager.Instance.ScaleTimeCtrll.ResumeCurrentTimeScale();
        }
    }
}

