using System.Collections.Generic;
using UnityEngine;

public class ButtonLayoutManager : Singleton<ButtonLayoutManager>
{
    [SerializeField]
    private SC_ButtonLayoutImageConfig buttonLayoutConfig;
    [SerializeField]
    private CustomKV<ButtonActionType, CustomKV<DeviceType, string>[]>[] buttonTextNameByAction;

    private List<ButtonLegendLayoutPanel> buttonLayoutPanels = new List<ButtonLegendLayoutPanel>();

    List<ButtonLegendUI> activeButtonLegendsUI = new List<ButtonLegendUI>();

    private LayoutKeyEnum previousKey = LayoutKeyEnum.None;
    private LayoutKeyEnum currentKey = LayoutKeyEnum.None;

    public string GetButtonText(ButtonActionType buttonAction)
    {
        if (buttonTextNameByAction == null || buttonTextNameByAction.Length == 0)
            return null;

        var currentDevice = SettingConfigManager.Instance.CurrentActiveDevice;

        foreach (var btnTextAction in buttonTextNameByAction)
        {
            if (btnTextAction.key != buttonAction)
            {
                continue;
            }

            foreach (var deviceBtnTextPair in btnTextAction.value)
            {
                if (deviceBtnTextPair.key == currentDevice)
                {
                    return deviceBtnTextPair.value;
                }
            }
        }

        return null;
    }

    public void ActivePreviousLayoutPanel()
    {
        if (previousKey != LayoutKeyEnum.None)
        {
            ActiveButtonLayoutPanel(previousKey);
        }
    }

    public void ActiveButtonLayoutPanel(LayoutKeyEnum key)
    {
        if (buttonLayoutPanels.Count == 0)
        {
            return;
        }

        previousKey = currentKey;
        activeButtonLegendsUI.Clear();
        foreach (var btnLayoutPanel in buttonLayoutPanels)
        {
            if (btnLayoutPanel == null)
            {
                continue;
            }

            bool isVisible = btnLayoutPanel.LegendKey == key;
            btnLayoutPanel.SetVisibility(isVisible);
            if (isVisible)
            {
                currentKey = key;
                activeButtonLegendsUI.AddRange(btnLayoutPanel.ButtonLegendsUI);
                Instance_OnDeviceChanged(SettingConfigManager.Instance.CurrentActiveDevice);
            }
        }
    }

    private void OnEnable()
    {
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.OnDeviceChanged += Instance_OnDeviceChanged;
        }
    }

    private void OnDisable()
    {
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.OnDeviceChanged -= Instance_OnDeviceChanged;
        }
    }

    private void Instance_OnDeviceChanged(DeviceType type)
    {
        foreach (var btnLegendUI in activeButtonLegendsUI)
        {
            UpdateButtonSprite(btnLegendUI, type);
        }
    }

    public void UpdateButtonSprite(ButtonLegendUI buttonLegendUI, DeviceType type)
    {
        var sprites = buttonLayoutConfig.GetButtonSprite(type, buttonLegendUI.ActionType);
        buttonLegendUI.UpdateLayout(sprites);
    }

    public void AddButtonLayoutPanel(ButtonLegendLayoutPanel buttonlayoutPanel)
    {
        buttonLayoutPanels.Add(buttonlayoutPanel);
    }

    public void RemoveButtonLayoutPanel(ButtonLegendLayoutPanel buttonlayoutPanel)
    {
        buttonLayoutPanels.Remove(buttonlayoutPanel);
    }
}

public enum LayoutKeyEnum
{
    None = 0,
    MainMenu = 1,
    ScrollPopup = 2,
    SelectionPopup = 3,
    GerenicNavegation = 4,
    SettingPopup = 5,
    Gameplay = 6,
    Gameover = 7,
    Layout8 = 8,
    Layout9 = 9,
    Layout10 = 10,
}