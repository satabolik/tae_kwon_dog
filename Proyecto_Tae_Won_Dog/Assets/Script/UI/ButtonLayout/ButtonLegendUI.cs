using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class ButtonLegendUI : MonoBehaviour
{
    [SerializeField]
    private bool standaloneListenLayout = false;
    [SerializeField]
    private bool autoVisibleOnUpdate = true;
    [SerializeField]
    private ButtonActionType actionType;
    [SerializeField]
    private Image imgButtonIcon;
    [SerializeField]
    private Image imgButtonIconAlt;
    [SerializeField]
    private GameObject Division;

    [SerializeField]
    private CanvasGroup canvasGP;

    private void Awake()
    {
        canvasGP = transform.parent.gameObject.GetOrAddComponent<CanvasGroup>();
        if (standaloneListenLayout)
        {
            Instance_OnDeviceChanged(SettingConfigManager.Instance.CurrentActiveDevice);
        }
    }

    private void OnEnable()
    {
        if (standaloneListenLayout && SettingConfigManager.Instance)
        {
            SettingConfigManager.Instance.OnDeviceChanged += Instance_OnDeviceChanged;
        }
    }

    private void OnDisable()
    {
        if (standaloneListenLayout && SettingConfigManager.Instance)
        {
            SettingConfigManager.Instance.OnDeviceChanged -= Instance_OnDeviceChanged;
        }
    }

    private void Instance_OnDeviceChanged(DeviceType deviceType)
    {
        ButtonLayoutManager.Instance.UpdateButtonSprite(this, deviceType);
    }

    public void SetActionType(ButtonActionType actionType)
    {
        this.actionType = actionType;
        Instance_OnDeviceChanged(SettingConfigManager.Instance.CurrentActiveDevice);
    }

    public ButtonActionType ActionType { get => actionType; }

    public void SetVisible(bool visible)
    {
        canvasGP.alpha = visible ? 1 : 0;
    }

    public void UpdateLayout(Sprite[] sprIcons)
    {
        if (sprIcons == null || sprIcons.Length == 0)
        {
            SetVisible(false);
            return;
        }

        imgButtonIcon.sprite = sprIcons[0];
        imgButtonIcon.SetNativeSize();

        bool hasAltIcon = (sprIcons.Length > 1) && (sprIcons[1] != null);

        if (hasAltIcon)
        {
            imgButtonIconAlt.sprite = sprIcons[1];
            imgButtonIconAlt.SetNativeSize();
        }

        imgButtonIconAlt.gameObject.SetActive(hasAltIcon);
        Division.SetActive(hasAltIcon);

        if (autoVisibleOnUpdate)
        {
            SetVisible(true);
        }
    }
}
