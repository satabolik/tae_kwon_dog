using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ButtonLegendLayoutPanel : MonoBehaviour
{
    [SerializeField]
    private LayoutKeyEnum legendKey;

    [SerializeField]
    private CanvasGroup legendGroup;

    [SerializeField]
    List<ButtonLegendUI> buttonLegendsUI;

    private int tweenUniqueId = -1;

    public void SetVisibility(bool visible)
    {
        if (visible)
        {
            tweenUniqueId = LeanTween.value(0f, 1f, 1f)
                .setIgnoreTimeScale(true)
                .setEase(LeanTweenType.easeOutBack)
                .setOnUpdate((v) => legendGroup.alpha = v)
                .uniqueId;


            Canvas.ForceUpdateCanvases();
            return;
        }

        if (tweenUniqueId != -1 && LeanTween.isTweening(tweenUniqueId))
        {
            LeanTween.cancel(tweenUniqueId);
            tweenUniqueId = -1;
        }

        legendGroup.alpha = 0;
    }

    private void OnEnable()
    {
        ButtonLayoutManager.Instance?.AddButtonLayoutPanel(this);
    }

    private void OnDisable()
    {
        ButtonLayoutManager.Instance?.RemoveButtonLayoutPanel(this);
        if (tweenUniqueId != -1 && LeanTween.isTweening(tweenUniqueId))
        {
            LeanTween.cancel(tweenUniqueId);
            tweenUniqueId = -1;
        }
    }

    public LayoutKeyEnum LegendKey { get => legendKey;}
    public List<ButtonLegendUI> ButtonLegendsUI { get => buttonLegendsUI; }
}
