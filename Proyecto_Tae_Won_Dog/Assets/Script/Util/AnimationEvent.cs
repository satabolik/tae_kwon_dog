﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    public event Action OnAnimationEnd;
    public event Action OnAnimationStart;


    private void OnAnimEnd()
    {
        if (OnAnimationEnd != null)
            OnAnimationEnd();
    }
    private void OnAnimStart()
    {
        if (OnAnimationStart != null)
            OnAnimationStart();
    }
}
