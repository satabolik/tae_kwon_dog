﻿using EZCameraShake;

public static class ShakeUtil
{
    public static CameraShakeInstance CameraShakeOnce(SC_ShakeConfig config)
    {
       return CameraShaker.Instance.ShakeOnce(
            config.Magnitude,
            config.Roughness,
            config.FadeInTime,
            config.FadeOutTime);
    }
}
