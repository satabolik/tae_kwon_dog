﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class BindingCollider2d : MonoBehaviour
{
    private Collider2D collider2d;
    public event Action<Collider2D> OnCollisionEnter;
   
    void Awake()
    {
        collider2d = GetComponent<Collider2D>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (OnCollisionEnter != null)
        {
            OnCollisionEnter(other);
        }
    }

}
