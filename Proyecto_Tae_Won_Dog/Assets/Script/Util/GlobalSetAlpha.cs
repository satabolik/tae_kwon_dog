﻿
using UnityEngine;
using UnityEngine.UI;

public static class GlobalSetAlpha
{
    public static void Set(SpriteRenderer sprite, float value)
    {
        var currentColor = sprite.color;
        currentColor.a = value;
        sprite.color = currentColor;
    }
    public static void Set(MaskableGraphic graphic, float value)
    {
        if (graphic == null)
        {
            Debug.LogWarning("Skip set alpha because graphic object es null");
            return;
        }
        var currentColor = graphic.color;
        currentColor.a = value;
        graphic.color = currentColor;
    }
    public static void Set(Button btn, float value)
    {
        Set(btn.image, value);
    }
}
