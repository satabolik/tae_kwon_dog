﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class CorutinePingPong
{
    private static int currentTween;
    private static bool isLoop;
    private static bool TriggerBreak;

    public static IEnumerator StartPingPong(float timeToEnd, float timeFade, Action<float> onUpdateValue)
    {
        isLoop = true;
        PingPongProcess(true, timeFade, onUpdateValue);
        yield return new WaitForSecondsRealtime(timeToEnd);
        isLoop = false;
        while (LeanTween.isTweening(currentTween))
            yield return null;
    }

    public static void ForceStop()
    {
        if (LeanTween.isTweening(currentTween))
        {
            LeanTween.cancel(currentTween);
            TriggerBreak = true;
        }
    }

    private static void PingPongProcess(bool fadeIn, float timeFade, Action<float> onUpdateValue)
    {
        if (TriggerBreak)
        {
            TriggerBreak = false;
            return;
        }
        var startAlpha = (fadeIn) ? 0f : 1f;
        var endAlpha = (!fadeIn) ? 0f : 1f;
        var tween = LeanTween.value(startAlpha, endAlpha, timeFade)
              .setIgnoreTimeScale(true)
              .setOnUpdate(onUpdateValue);
        if (isLoop || (!isLoop && fadeIn))
            tween.setOnComplete(() => PingPongProcess(!fadeIn, timeFade, onUpdateValue));
        currentTween =  tween.uniqueId;
    }
}
