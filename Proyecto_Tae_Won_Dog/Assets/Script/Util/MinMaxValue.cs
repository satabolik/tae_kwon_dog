﻿using System;
using UnityEngine;
[Serializable]
public struct MinMaxValue
{
    [Range(-10000f, 10000f)]
    public float minValue;
    [Range(-10000f, 10000f)]
    public float maxValue;

    public float GetRng()
    {
        return UnityEngine.Random.Range(minValue, maxValue);
    }
}
[Serializable]
public struct MinMaxValueInt
{
    [Range(-10000, 10000)]
    public int minValue;
    [Range(-10000, 10000)]
    public int maxValue;

    public int GetRng()
    {
        return UnityEngine.Random.Range(minValue, maxValue);
    }
}
