using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

[System.Serializable]
public struct CustomKV<TKey, TValue>
{
    public TKey key;
    public TValue value;
}

