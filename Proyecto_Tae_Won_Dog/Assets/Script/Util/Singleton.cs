﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;
    /**
      Returns the instance of this singleton.
   */

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = (T)FindObjectOfType(typeof(T));
            }

            return instance;
        }
    }

    public static bool ThereIsAnother()
    {
        var objectType = FindObjectsOfType(typeof(T));
        return objectType.Length > 1;
    }

    protected virtual void Awake()
    {
        if (ThereIsAnother())
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }
}
