﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CorutineCallbackEventTrigger
{
    private static bool callbackTrigger = false;

    public static IEnumerator WaitJumpEvent()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnPlayerJump += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnPlayerJump -= ActiveCallback;
        callbackTrigger = false;
    }

    public static IEnumerator WaitAttackEvent()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnPlayerAttack += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnPlayerAttack -= ActiveCallback;
        callbackTrigger = false;
    }

    public static IEnumerator WaitPlayerGetHitEvent()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnPlayerGetHit += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnPlayerGetHit -= ActiveCallback;
        callbackTrigger = false;
    }

    public static IEnumerator WaitAttackPlankEvent()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnAttackPlank += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnAttackPlank -= ActiveCallback;
        callbackTrigger = false;
    }
    public static IEnumerator WaitPlayerFallFloorEvent()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnPlayerFallOnFloor += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnPlayerFallOnFloor -= ActiveCallback;
        callbackTrigger = false;
    }
    public static IEnumerator WaitPlayerRun()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnStartRun += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnStartRun -= ActiveCallback;
        callbackTrigger = false;
    }

    public static IEnumerator WaitSpecialActive()
    {
        callbackTrigger = false;
        GlobaGameplaylEvents.OnActiveSpecial += ActiveCallback;
        while (!callbackTrigger)
            yield return null;
        GlobaGameplaylEvents.OnActiveSpecial -= ActiveCallback;
        callbackTrigger = false;
    }


    private static void ActiveCallback<T1,T2>(T1 value1, T2 value2)
    {
        ActiveCallback();
    }
    private static void ActiveCallback<T>(T value)
    {
        ActiveCallback();
    }
    private static void ActiveCallback()
    {
        callbackTrigger = true;
    }
}
