﻿
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public struct TrophyData
{
    public string Id { get; set; }
    public int ScoreNeeded { get; set; }
    public bool IsCompleted { get; set; }

    public TrophyData(string id, int scoreNeeded)
    {
        Id = id;
        ScoreNeeded = scoreNeeded;
        IsCompleted = false;
    }
}

public abstract class TKDSocialBase
{
    private const float FULL_PROGRESS = 100.0f;
    public virtual int GetScoreWood() { return 1000; }
    public virtual int GetScoreCooper() { return 2500; }
    public virtual int GetScoreSilver() { return 4500; }
    public virtual int GetScoreGold() { return 7000; }
    public virtual int GetScoreDiamond() { return 10000; }
    public virtual int GetScoreHit() { return 30; }
    public virtual int GetPlankCount() { return 10000; }

    public abstract string GetLeaderboardId();
    public abstract string GeTrophyWoodId();
    public abstract string GeTrophyCooperId();
    public abstract string GeTrophySilverId();
    public abstract string GeTrophyGoldId();
    public abstract string GeTrophyDiamondId();
    public abstract string GeTrophyHitId();
    public abstract string GeTrophyPlanksId();

    protected Dictionary<string, TrophyData> dicTrophyDataById;

    public virtual void Init()
    {
        dicTrophyDataById = new Dictionary<string, TrophyData>
        {
            { GeTrophyWoodId(), GetTrophyData(GeTrophyWoodId()) },
            { GeTrophyCooperId(), GetTrophyData(GeTrophyCooperId()) },
            { GeTrophySilverId(), GetTrophyData(GeTrophySilverId()) },
            { GeTrophyGoldId(), GetTrophyData(GeTrophyGoldId()) },
            { GeTrophyDiamondId(), GetTrophyData(GeTrophyDiamondId()) },
            { GeTrophyHitId(), GetTrophyData(GeTrophyHitId()) },
            { GeTrophyPlanksId(), GetTrophyData(GeTrophyPlanksId()) }
        };

        Authenticate();
    }

    protected virtual void Authenticate()
    {
        Social.localUser.Authenticate(ProcessAuthentication);
    }

    protected List<string> GetAllScoresArchivementId()
    {
        List<string> scoresArchivementId = new List<string>(dicTrophyDataById.Keys);
        scoresArchivementId.Remove(GeTrophyHitId());
        scoresArchivementId.Remove(GeTrophyPlanksId());
        return scoresArchivementId;
    }

    protected virtual TrophyData GetTrophyData(string id)
    {
        if (dicTrophyDataById != null && dicTrophyDataById.ContainsKey(id))
        {
            return dicTrophyDataById[id];
        }

        if (id == GeTrophyWoodId()) { return new TrophyData(id, GetScoreWood()); }
        if (id == GeTrophyCooperId()) { return new TrophyData(id, GetScoreCooper()); }
        if (id == GeTrophySilverId()) { return new TrophyData(id, GetScoreSilver()); }
        if (id == GeTrophyGoldId()) { return new TrophyData(id, GetScoreGold()); }
        if (id == GeTrophyDiamondId()) { return new TrophyData(id, GetScoreDiamond()); }
        if (id == GeTrophyHitId()) { return new TrophyData(id, GetScoreHit()); }
        if (id == GeTrophyPlanksId()) { return new TrophyData(id, GetPlankCount()); }

        return default;
    }

    protected virtual void ProcessAuthentication(bool success)
    {
        if (success)
        {
            Debug.Log($"Authenticated, checking achievements, user id: {GetSocialUserId()}");
            LoadPlayerScore(null);
            LoadAchievements();
        }
        else
            Debug.Log("Failed to authenticate");
    }

    protected virtual void LoadAchievements()
    {
        Social.LoadAchievements(OnAchievementsLoaded);
    }


    protected void OnAchievementsLoaded(IAchievement[] achievements)
    {
        if (achievements.Length == 0)
        {
            Debug.Log("Error: no achievements found");
            return;
        }

        Debug.Log("Got " + achievements.Length + " achievements");
        foreach (var arch in achievements)
        {
            if (!dicTrophyDataById.ContainsKey(arch.id))
            {
                Debug.LogWarning($"No defined archivement with id : {arch.id}");
                continue;
            }

            UpdateCompletedStateTrophy(arch.id, arch.completed);
        }
    }

    protected virtual void UpdateCompletedStateTrophy(string trophyId, bool isCompleted)
    {
        var trophyData = dicTrophyDataById[trophyId];
        trophyData.IsCompleted = isCompleted;
        dicTrophyDataById[trophyId] = trophyData;
    }

    public virtual void LoadPlayerScore(Action<IScore> scoreRankCallback)
    {
        Social.LoadScores(GetLeaderboardId(), (scores) =>
        {
            if (scoreRankCallback == null)
            {
                return;
            }

            OnGetPlayersRank(scores, scoreRankCallback);
        });
    }

    protected virtual void OnGetPlayersRank(IScore[] scores, Action<IScore> scoreRankCallback)
    {
        if (scores.Length == 0)
        {
            scoreRankCallback(null);
            return;
        }

        Debug.Log("Got " + scores.Length + " leaderboard scores");

        IScore playerScore = scores.Select(score => score)
                               .Where(score => score.userID == GetSocialUserId())
                               .FirstOrDefault();

        scoreRankCallback(playerScore);
    }

    public virtual void CheckArchivementProgress(int scoreReported, System.Action OnEndCheck)
    {
        if (!IsAuthenticated())
        {
            OnEndCheck();
            return;
        }

        Queue<string> pendingArchivementsId = new Queue<string>(GetAllScoresArchivementId());
        string nextId = pendingArchivementsId.Dequeue();

        ProcessListArchivement(pendingArchivementsId, nextId, scoreReported, OnEndCheck);
    }

    protected virtual void ProcessListArchivement(Queue<string> pendingTrophysId, string checkTrophyId, int scoreReported, System.Action onProcessEnd)
    {
        if (pendingTrophysId.Count == 0) 
        {
            TryReportArchivementScore(checkTrophyId, scoreReported, onProcessEnd);
            return;
        }

        string nextId = pendingTrophysId.Dequeue();
        Action NextProcess = () => ProcessListArchivement(pendingTrophysId, nextId, scoreReported, onProcessEnd);
        TryReportArchivementScore(checkTrophyId, scoreReported, NextProcess);
    }

    public virtual void TryReportArchivementScore(string idTrophy, int scoreReported, Action callback)
    {
        if (!dicTrophyDataById.ContainsKey(idTrophy))
        {
            Debug.LogError($"Error, archivement data not fount, id: {idTrophy}");
            callback();
            return;
        }

        var trophyData = dicTrophyDataById[idTrophy];
        if (trophyData.IsCompleted) 
        {
            Debug.Log($"Score achivement: {idTrophy} already completed");
            callback();
            return;
        }

        if (scoreReported < trophyData.ScoreNeeded)
        {
            Debug.Log($"Skip score achivement with id: {idTrophy} , score reported: {scoreReported}, needed: {trophyData.ScoreNeeded}");
            callback();
            return;
        }

        ReportAchievementCompleted(idTrophy, callback);
    }

    protected virtual void ReportAchievementCompleted(string idTrophy, Action OnReportedcallback)
    {
        Social.ReportProgress(idTrophy, FULL_PROGRESS, (bool success) =>
        {
            UpdateCompletedStateTrophy(idTrophy, success);
            OnReportedcallback();
        });
    }


    public virtual void TryReportIncrementalArchivement(string trophyId,  int incrementalValue)
    {
        if (!dicTrophyDataById.ContainsKey(trophyId) || !IsAuthenticated())
        {
            return;
        }

        ReportIncrementalArchivement(trophyId, incrementalValue, (bool success) =>
        {
            if (success)
            {
                LoadAchievements();
            }
        });
    }

    protected abstract void ReportIncrementalArchivement(string trophyId, int incrementalValue, Action<bool> onCompleted);

    public virtual void UpdateScoreLeaderBoard(int newScore, System.Action onComplete)
    {
        Social.ReportScore(newScore, GetLeaderboardId(), (bool success) =>
        {
            if (success)
            {
                Debug.Log($"UpdateScoreLeaderBoard is SUCCESS: new score : {newScore}");
                if (onComplete != null)
                    onComplete();
            }
            else
            {
                Debug.LogWarning($"PROBLEM UPDATE DE SCORE");
            }
        });
    }

    public virtual void ShowLeaderboard()
    {
        if (IsAuthenticated())
        {
            Social.ShowLeaderboardUI();
        }
    }
    public virtual void ShowArchievement()
    {
        if (IsAuthenticated())
        {
            Social.ShowAchievementsUI();
        }
    }


    protected virtual bool IsAuthenticated()
    {
        return Social.localUser.authenticated;
    }

    protected virtual string GetSocialUserId() => Social.localUser.id;
}
