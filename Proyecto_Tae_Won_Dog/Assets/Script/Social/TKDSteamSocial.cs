﻿#if UNITY_STANDALONE_WIN
using System;
using UnityEngine;
using Steamworks;
using UnityEngine.SocialPlatforms;
using System.Collections.Generic;
using static UnityEngine.UIElements.UxmlAttributeDescription;
using System.Linq;

public class TKDSteamSocial : TKDSocialBase
{
    CallResult<LeaderboardFindResult_t> LeaderboardFindResultCallResult;
    CallResult<LeaderboardScoresDownloaded_t> LeaderboardScoresDownloadedCallResult;
    CallResult<LeaderboardScoreUploaded_t> LeaderboardScoresUploadedCallResult;
    CallResult<UserAchievementIconFetched_t> AchievementIconFetchedCallResult;

    protected Callback<GameOverlayActivated_t> m_GameOverlayActivated;

    Action OnLeaderboardUploadedCompleted;
    Action<IScore> OnScoreRankLoadedCompleted;

    SteamLeaderboard_t leaderboardGeneral;

    private Dictionary<string, string> StatValueByTrophyId = new Dictionary<string, string>();

    protected LeaderboardHandler leaderboardHandler;
    protected TrophyPopupHandler trophyHandler;
    protected Action<List<TrophyDetailData>> OnGeneratedTrophyDetailData;

    private bool isTimeAlreadyPause;

    public TKDSteamSocial(LeaderboardHandler leaderboardHandler, TrophyPopupHandler trophyHandler)
    {
        this.leaderboardHandler = leaderboardHandler;
        this.trophyHandler = trophyHandler;
    }

    public override void Init()
    {
        StatValueByTrophyId.Add(GeTrophyPlanksId(), "Planks_Hit_Count");

        LeaderboardFindResultCallResult = CallResult<LeaderboardFindResult_t>.Create(OnSteamLeaderboardCallback);
        LeaderboardScoresDownloadedCallResult = CallResult<LeaderboardScoresDownloaded_t>.Create(OnLeaderboardScoresDownloaded);
        LeaderboardScoresUploadedCallResult = CallResult<LeaderboardScoreUploaded_t>.Create(OnLeaderboardScoresUploaded);
        AchievementIconFetchedCallResult = CallResult<UserAchievementIconFetched_t>.Create(OnAchievementIconFetchedCallback);

        m_GameOverlayActivated = Callback<GameOverlayActivated_t>.Create(OnGameOverlayActivated);

        base.Init();

        trophyHandler.InitializeTrophySlots(dicTrophyDataById.Keys.ToList());
    }

    private void OnGameOverlayActivated(GameOverlayActivated_t pCallback)
    {
        if (pCallback.m_bActive != 0)
        {
            Debug.Log("Steam Overlay has been activated");
            isTimeAlreadyPause = Mathf.Approximately(Time.timeScale, 0f);
            if (GameplayManager.Instance != null && !isTimeAlreadyPause)
            {
                GameplayManager.Instance.ScaleTimeCtrll.StopTimeScale();
            }
        }
        else
        {
            Debug.Log("Steam Overlay has been closed");
            if (GameplayManager.Instance != null && !isTimeAlreadyPause)
            {
                GameplayManager.Instance.ScaleTimeCtrll.ResumeCurrentTimeScale();
            }
        }
    }

    void OnAchievementIconFetchedCallback(UserAchievementIconFetched_t callback, bool bIOFailure)
    {
        Debug.Log($"callback: {callback}");
    }

    void OnSteamLeaderboardCallback(LeaderboardFindResult_t callback, bool bIOFailure)
    {
        leaderboardGeneral = callback.m_hSteamLeaderboard;
        SteamAPICall_t handle = SteamUserStats.DownloadLeaderboardEntries(leaderboardGeneral, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 20);
        LeaderboardScoresDownloadedCallResult.Set(handle);
    }

    void OnLeaderboardScoresDownloaded(LeaderboardScoresDownloaded_t pCallback, bool bIOFailure)
    {
        Debug.Log("[" + LeaderboardScoresDownloaded_t.k_iCallback + " - LeaderboardScoresDownloaded] - " + pCallback.m_hSteamLeaderboard + " -- " + pCallback.m_hSteamLeaderboardEntries + " -- " + pCallback.m_cEntryCount);

        SteamLeaderboardEntries_t m_SteamLeaderboardEntries = pCallback.m_hSteamLeaderboardEntries;
        List<LeaderboardPlayerRecordData> playersRecord = new List<LeaderboardPlayerRecordData>();

        if (pCallback.m_cEntryCount == 0)
        {
            if (OnScoreRankLoadedCompleted != null)
            {
                OnScoreRankLoadedCompleted(null);
            }
        }

        for (int i = 0; i < pCallback.m_cEntryCount; i++)
        {
            if (!SteamUserStats.GetDownloadedLeaderboardEntry(m_SteamLeaderboardEntries, i, out var LeaderboardEntry, null, 0))
            {
                continue;
            }

            var leaderboardData = GetLeaderboardRecordData(LeaderboardEntry);
            if (LeaderboardEntry.m_steamIDUser == SteamUser.GetSteamID())
            {
                leaderboardData.isLocalUser = true;
                if (OnScoreRankLoadedCompleted != null)
                {
                    SteamUserScore score = new SteamUserScore(LeaderboardEntry.m_nGlobalRank, LeaderboardEntry.m_nScore);
                    OnScoreRankLoadedCompleted(score);
                }
            }

            playersRecord.Add(leaderboardData);
        }

#if UNITY_EDITOR
        for (int i = 1; i < 20; i++)
        {
            playersRecord.Add(new LeaderboardPlayerRecordData()
            {
                playerRank = i + 1,
                playerName = $"dummy_{i + 1}",
                playerScore = 5 - i
            });
        }
#endif

        leaderboardHandler.UpdateLeaderboardData(playersRecord);

    }


    void OnLeaderboardScoresUploaded(LeaderboardScoreUploaded_t callback, bool bIOFailure)
    {
        if (OnLeaderboardUploadedCompleted != null)
        {
            OnLeaderboardUploadedCompleted();
            OnLeaderboardUploadedCompleted = null;
        }
    }


    protected override void Authenticate()
    {
        ProcessAuthentication(SteamManager.Initialized);
    }

    protected override void LoadAchievements()
    {
        if (SteamUserStats.GetNumAchievements() == 0)
        {
            Debug.LogError("Not found steam achievements");
            return;
        }

        List<IAchievement> achievements = new List<IAchievement>();
        List<TrophyDetailData> trophiesDetailData = new List<TrophyDetailData>();

        for (uint i = 0; i < SteamUserStats.GetNumAchievements(); i++)
        {
            IAchievement achievement = Social.CreateAchievement();
            achievement.id = SteamUserStats.GetAchievementName(i);

            bool success = false;
            SteamUserStats.GetAchievement(achievement.id, out success);
            achievement.percentCompleted = success ? 100.0f : 0f;
            achievements.Add(achievement);

            trophiesDetailData.Add(GenerateTrophyDetailData(achievement.id));
        }

        if (OnGeneratedTrophyDetailData != null)
        {
            OnGeneratedTrophyDetailData(trophiesDetailData);
        }

        OnAchievementsLoaded(achievements.ToArray());
    }

    protected TrophyDetailData GenerateTrophyDetailData(string achievementId)
    {
        TrophyDetailData trophyDetail = new TrophyDetailData();

        trophyDetail.id = achievementId;
        trophyDetail.name = SteamUserStats.GetAchievementDisplayAttribute(achievementId, "name");
        trophyDetail.description = SteamUserStats.GetAchievementDisplayAttribute(achievementId, "desc");
        
        SteamUserStats.GetAchievement(achievementId, out bool achieved);
        trophyDetail.isAchieved = achieved;
        SteamUserStats.GetAchievementProgressLimits(achievementId, out float minProgress, out float maxProgress);

        trophyDetail.progress = minProgress;


        int idSteamImage = SteamUserStats.GetAchievementIcon(achievementId);

        if (idSteamImage != 0)
        {
            trophyDetail.icon = GetSteamImage(idSteamImage);
        }



        return trophyDetail;
    }

    public override void LoadPlayerScore(Action<IScore> scoreRankCallback)
    {
        if (!SteamManager.Initialized)
        {
            Debug.LogError("SteamManager isn't initialized!");
            scoreRankCallback(null);
            return;
        }

        OnScoreRankLoadedCompleted = scoreRankCallback;

        SteamAPICall_t handle = SteamUserStats.FindLeaderboard(GetLeaderboardId());
        LeaderboardFindResultCallResult.Set(handle);
    }


    public override void UpdateScoreLeaderBoard(int newScore, Action onComplete)
    {
        OnLeaderboardUploadedCompleted = onComplete;
        ELeaderboardUploadScoreMethod mode = ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest;
        int[] pScoreDetails = { };
        SteamAPICall_t handle = SteamUserStats.UploadLeaderboardScore(leaderboardGeneral, mode, newScore, pScoreDetails, pScoreDetails.Length);
        LeaderboardScoresUploadedCallResult.Set(handle);
    }

    protected override void ReportAchievementCompleted(string idTrophy, Action OnReportedcallback)
    {
        SteamUserStats.SetAchievement(idTrophy);
        SteamUserStats.StoreStats();
        OnReportedcallback();
    }

    protected override void ReportIncrementalArchivement(string trophyId, int incrementalValue, Action<bool> onCompleted)
    {
        if (!StatValueByTrophyId.ContainsKey(trophyId))
        {
            Debug.LogError($"Not found stat id with trophy id: {trophyId}");
            return;
        }

        string statID = StatValueByTrophyId[trophyId];

        int statCount;
        SteamUserStats.GetStat(statID,out statCount);
        statCount += incrementalValue;
        SteamUserStats.SetStat(statID, statCount);
        SteamUserStats.StoreStats();

        var trophyData = GetTrophyData(trophyId);
        if (statCount >= trophyData.ScoreNeeded)
        {
            SteamUserStats.SetAchievement(trophyId);
            SteamUserStats.StoreStats();
        }

        onCompleted?.Invoke(true);
    }

    protected override bool IsAuthenticated()
    {
        return SteamManager.Initialized;
    }

    public override void ShowLeaderboard()
    {
        leaderboardHandler.ShowPopUp(true);

        Action<IScore> action = (score) => { };

        LoadPlayerScore(action);
    }

    public override void ShowArchievement()
    {
        trophyHandler.ShowPopUp(true);

        OnGeneratedTrophyDetailData = (trophiesData) =>
        {
            trophyHandler.UpdateTrophiesDataList(trophiesData);
            OnGeneratedTrophyDetailData = null;
        };

        LoadAchievements();
    }

    private LeaderboardPlayerRecordData GetLeaderboardRecordData(LeaderboardEntry_t playerEntry)
    {
        LeaderboardPlayerRecordData playerRecordData = new LeaderboardPlayerRecordData();
        playerRecordData.playerRank = playerEntry.m_nGlobalRank;
        playerRecordData.playerScore = playerEntry.m_nScore;
        playerRecordData.playerAvatar = GetAvatar(playerEntry.m_steamIDUser);
        playerRecordData.playerName = SteamFriends.GetFriendPersonaName(playerEntry.m_steamIDUser);
        return playerRecordData;
    }

    public static Texture2D GetAvatar(CSteamID id)
    {
        // See docs for when this is valid. Also for other sizes.
        int avatar_id = SteamFriends.GetMediumFriendAvatar(id);
        return GetSteamImage(avatar_id);
    }

    public static Texture2D GetSteamImage(int idImage)
    {
        Texture2D steamImage = null;

        bool isValidImage = SteamUtils.GetImageSize(idImage, out uint width, out uint height);

        if (!isValidImage)
        {
            return null;
        }

        uint imageBuffer = width * height * 4;
        var image = new byte[imageBuffer];

        bool isValidRGBA = SteamUtils.GetImageRGBA(idImage, image, (int)imageBuffer);
        if (!isValidRGBA)
        {
            return null;
        }

        steamImage = new Texture2D((int)width, (int)height, TextureFormat.RGBA32, false, true);
        steamImage.LoadRawTextureData(image);
        // Unity expects texture data to start from "bottom", so avatar
        // will be upside down. You could change the pixels here to fix it.
        steamImage.Apply();

        return steamImage;
    }

    public override string GetLeaderboardId() => "Leaderboard_general_v2";
    public override string GeTrophyCooperId() => "TrophyCopper";
    public override string GeTrophyDiamondId() => "TrophyRedDiamond";
    public override string GeTrophyGoldId() => "TrophyGolden";
    public override string GeTrophyHitId() => "TrophyChuckPet";
    public override string GeTrophyPlanksId() => "TrophyPlanks";
    public override string GeTrophySilverId() => "TrophySilver";
    public override string GeTrophyWoodId() => "TrophyWood";
}

struct SteamUserScore : IScore
{
    ulong steamID;
    int steamRank;
    long steamScorevalue;

    public SteamUserScore(int steamRank, long steamScorevalue) : this()
    {
        this.steamRank = steamRank;
        this.steamScorevalue = steamScorevalue;
    }

    public string leaderboardID { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public long value { get => steamScorevalue; set => steamScorevalue = value; }

    public DateTime date => throw new NotImplementedException();

    public string formattedValue => throw new NotImplementedException();

    public string userID => steamID.ToString();

    public int rank => steamRank;

    public void ReportScore(Action<bool> callback)
    {
        throw new NotImplementedException();
    }
}

#endif