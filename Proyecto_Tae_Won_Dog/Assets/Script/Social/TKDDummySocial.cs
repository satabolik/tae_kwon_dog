﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TKDDummySocial : TKDSocialBase
{
    public override string GetLeaderboardId() => "1";
    public override string GeTrophyCooperId() => "2";
    public override string GeTrophyDiamondId() => "3";
    public override string GeTrophyGoldId() => "4";
    public override string GeTrophyHitId() => "5";
    public override string GeTrophyPlanksId() => "6";
    public override string GeTrophySilverId() => "7";
    public override string GeTrophyWoodId() => "8";
    protected override void ReportIncrementalArchivement(string trophyId, int incrementalValue, Action<bool> onCompleted)
    {
        return;
    }
}
