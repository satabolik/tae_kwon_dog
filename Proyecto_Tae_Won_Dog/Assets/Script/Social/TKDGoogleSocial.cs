﻿#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using UnityEngine;

public class TKDGoogleSocial : TKDSocialBase
{
    private PlayGamesClientConfiguration config;

    public override void Init()
    {
        config = new PlayGamesClientConfiguration.Builder().Build();
        PlayGamesPlatform.InitializeInstance(config);
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = Debug.isDebugBuild;
        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        base.Init();
    }

    public override string GetLeaderboardId() => GPGSIds.leaderboard_puntuacin_mxima;
    public override string GeTrophyCooperId() => GPGSIds.achievement_trofeo_de_cobre;
    public override string GeTrophyDiamondId() => GPGSIds.achievement_trofeo_de_diamante_rojo;
    public override string GeTrophyGoldId() => GPGSIds.achievement_trofeo_de_oro;
    public override string GeTrophyHitId() => GPGSIds.achievement_mascota_de_chuck;
    public override string GeTrophyPlanksId() => GPGSIds.achievement_leador;
    public override string GeTrophySilverId() => GPGSIds.achievement_trofeo_de_plata;
    public override string GeTrophyWoodId() => GPGSIds.achievement_trofeo_de_madera;

    protected override void ReportIncrementalArchivement(string trophyId, int incrementalValue, Action<bool> onCompleted)
    {
        PlayGamesPlatform.Instance.IncrementAchievement(trophyId, incrementalValue, onCompleted);
    }
}
#endif