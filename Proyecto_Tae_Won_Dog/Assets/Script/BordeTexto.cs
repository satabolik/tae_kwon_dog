﻿using UnityEngine;
using System.Collections;

public class BordeTexto : MonoBehaviour {
	public Color colorDeBorde = new Color();
	public float grosor = 0.1f;
	private const int EXTREMIDADES_BORDE = 4;
	private GameObject [] objetosDeBordeTexto;
	private TextMesh textoBorde;
	private TextMesh textoOriginal;
	private GameObject objetoVacio;

	// Use this for initialization
	void Start () {
		textoOriginal = this.GetComponent<TextMesh> ();
		if (textoOriginal.font.dynamic){
			objetosDeBordeTexto = new GameObject[EXTREMIDADES_BORDE];
			crearCopiaParaBorde ();
		}

	}
	private void crearCopiaParaBorde(){
		Vector3 posicionFondo = new Vector3 (transform.position.x,transform.position.y,transform.position.z + 1f);
		Material material = textoOriginal.gameObject.GetComponent<MeshRenderer>().material;;
		for (int i = 0; i< objetosDeBordeTexto.Length; i++) {
			Vector3 nuevaPosicion = NuevaPosicion(posicionFondo,i);
			objetosDeBordeTexto[i] = new GameObject("Borde_" + i.ToString(), typeof(TextMesh));
			objetosDeBordeTexto[i].GetComponent<TextMesh>().color = colorDeBorde;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().font = textoOriginal.font;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().anchor = textoOriginal.anchor;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().alignment = textoOriginal.alignment;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().fontSize = textoOriginal.fontSize;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().characterSize = 0f;//textoOriginal.characterSize;
			objetosDeBordeTexto[i].GetComponent<MeshRenderer>().material = new Material(material);
			objetosDeBordeTexto[i].transform.localScale = transform.localScale;
			objetosDeBordeTexto[i].transform.position = nuevaPosicion;
			objetosDeBordeTexto[i].transform.parent = this.gameObject.transform;
			objetosDeBordeTexto[i].GetComponent<Renderer>().sortingLayerName = GetComponent<Renderer>().sortingLayerName;
			objetosDeBordeTexto[i].GetComponent<Renderer>().sortingOrder = GetComponent<Renderer>().sortingOrder;
		}
	
		textoBorde = objetosDeBordeTexto [0].GetComponent<TextMesh> ();
	}
	private Vector3 NuevaPosicion(Vector3 posicionBase,int indicePosicion){
		Vector3 nuevoVector = new Vector3 ();
		nuevoVector.x = posicionBase.x;
		nuevoVector.y = posicionBase.y;
		nuevoVector.z = posicionBase.z;
		switch (indicePosicion) {
			case 0:
			nuevoVector.y += grosor;
			break;
			case 1:
			nuevoVector.x += grosor;
			break;
			case 2:
			nuevoVector.y -= grosor;
			break;
			case 3:
			nuevoVector.x -= grosor;
			break;
		}
		return nuevoVector;
	}
	void AsignarTextoBorde(){
		for (int i = 0; i< objetosDeBordeTexto.Length; i++) {
			objetosDeBordeTexto[i].GetComponent<TextMesh>().text = textoOriginal.text;
			objetosDeBordeTexto[i].GetComponent<TextMesh>().characterSize = textoOriginal.characterSize;
		}
	}
	// Update is called once per frame
	void Update () {
		if (textoOriginal.font.dynamic) {
			if (textoBorde.text != textoOriginal.text || textoBorde.characterSize != textoOriginal.characterSize){
				AsignarTextoBorde();
			}

		}
	}
}
