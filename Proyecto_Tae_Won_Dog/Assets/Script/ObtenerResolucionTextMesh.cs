﻿using UnityEngine;
using System.Collections;

public class ObtenerResolucionTextMesh : MonoBehaviour {
	void Start () {
		TextMesh textMesh = gameObject.GetComponent<TextMesh>();
		textMesh.text = Screen.currentResolution.width + "x" + Screen.currentResolution.height;
	}

}
