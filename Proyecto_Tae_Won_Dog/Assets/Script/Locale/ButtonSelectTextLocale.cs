using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSelectTextLocale : TextLocale, ISelectHandler, IDeselectHandler
{

    protected override void Awake()
    {
        base.Awake();
        DisplayText(false);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        DisplayText(false);
    }

    public void OnSelect(BaseEventData eventData)
    {
        DisplayText(true);
    }

    private void DisplayText(bool display)
    {
        foreach (var textPair in pairTextLocale)
        {
            textPair.txtText.enabled = display;
        }
    }
}
