﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextLocale : MonoBehaviour
{
    [SerializeField]
    protected PairKeyTextUI[] pairTextLocale;

    protected virtual void Awake()
    {
        UpdateText();
    }
    private void UpdateText()
    {
        if (pairTextLocale == null)
            return;
        for (int i = 0; i < pairTextLocale.Length; i++)
        {
            var pair = pairTextLocale[i];
            var text = LocalizationManager.Instance.GetText(pair.keyLocale);
            pair.txtText.text = (text == null) ? $"#{pair.keyLocale}#" : text;
        }
    }

    protected virtual void OnEnable()
    {
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocaleUpdate += UpdateText;
    }
    protected virtual void OnDisable()
    {
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocaleUpdate -= UpdateText;
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (pairTextLocale == null)
            return;

        if (LocalizationManager.Instance && pairTextLocale.Length > 0)
        {
            for (int i = 0; i < pairTextLocale.Length; i++)
            {
                var pair = pairTextLocale[i];
                if (!pair.keyLocale.Contains('*'))
                {
                    continue;
                }

                pair.keyLocale = pair.keyLocale.Replace("*", "");
                string keyFound = LocalizationManager.Instance.TryGetNearLocaleKey(pair.keyLocale);
                if (keyFound == null)
                {
                    Debug.LogWarning($"Not found matches locale key : [{pair.keyLocale}]");
                    continue;
                }

                pair.keyLocale = keyFound;
                pairTextLocale[i] = pair;
            }
        }
    }
#endif

    [System.Serializable]
    public struct PairKeyTextUI
    {
        public string keyLocale;
        public TMP_Text txtText;
    }
}
