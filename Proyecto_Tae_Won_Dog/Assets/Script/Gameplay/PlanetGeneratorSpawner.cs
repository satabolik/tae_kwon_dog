﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetGeneratorSpawner : MonoBehaviour
{
    [SerializeField]
    private PlanetGraphicHandler planetPrefab;
    [SerializeField]
    private ColorsTemplatePlanet[] colorsTemplates;
    [SerializeField]
    private MinMaxValueInt minMaxPlanet;
    [SerializeField]
    private MinMaxValue minMaxSize;
    [SerializeField]
    private Transform minPos;
    [SerializeField]
    private Transform maxPos;
    [SerializeField]
    private float distancePlanetsCrit = 0.5f;
    private List<Vector3> positionsUsedPlanet = new List<Vector3>();

    private void Awake()
    {
        planetPrefab.gameObject.SetActive(false);
    }

    private void Start()
    {
        GeneratePlanets();
    }

    private void GeneratePlanets()
    {
        var planetsCount = minMaxPlanet.GetRng();
        for (int i = 0; i < planetsCount; i++)
        {
            var pos = Vector3.zero;
            while (true)
            {
                pos.x = Random.Range(minPos.localPosition.x, maxPos.localPosition.x);
                pos.y = Random.Range(minPos.localPosition.y, maxPos.localPosition.y);
                if (positionsUsedPlanet.Count == 0)
                    break;
                bool valid = true;
                foreach (var posUsed in positionsUsedPlanet)
                {
                    var dist = (posUsed - pos).magnitude;
                    if (dist <= distancePlanetsCrit)
                    {
                        Debug.Log($"planet dist: {dist}, pos: {pos}, posUsed: {posUsed}");
                        valid = false;
                        break;
                    }

                }
                if (valid)
                    break;

            }

            CreatePlanet(minMaxSize.GetRng(), pos);
        }

    }

    private void CreatePlanet(float size, Vector2 position)
    {
        var planet = Instantiate(planetPrefab, transform);
        planet.gameObject.SetActive(true);
        var colorTemplate = colorsTemplates[Random.Range(0, colorsTemplates.Length)];
        planet.DrawPlanet(colorTemplate);
        planet.transform.localScale = Vector3.one * size;
        planet.transform.localPosition = position;
        planet.name = $"planet_{position}";
        positionsUsedPlanet.Add(position);
    }


}
[System.Serializable]
public struct ColorsTemplatePlanet
{
    public TypePlanet type;
    public Color colorBase;
    public Color colorMark1;
    public Color colorMark2;
}
public enum TypePlanet
{
    FullMark = 0,
    TwoMark = 1
}
