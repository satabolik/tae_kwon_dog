﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpHandler : MonoBehaviour
{
    [SerializeField]
    private Image imgHL;
    [SerializeField]
    private float timeValueChange = 0.25f;
    [SerializeField]
    private float fillValue = 0.9999f;
    [SerializeField]
    private Image imgRadioBar;
    [SerializeField]
    private Image imgRay;
    [SerializeField]
    private Button btnPressPowerUp;
    [SerializeField]
    private Animator animator;
    private CanvasGroup canvasGP;

    private const string ANIM_BOOL_AVAILABLE = "Available";
    private const string ANIM_BOOL_TIMEOUT = "TimeOut";
    private float timeout;
    public event System.Action OnClickEvt = delegate { };
    private int tweenTimeOut;
    private int tweenValueChange;

    private void Awake()
    {
        canvasGP = GetComponent<CanvasGroup>();
        btnPressPowerUp.onClick.AddListener(OnClick);
        btnPressPowerUp.interactable = false;
        imgHL.enabled = false;
    }

    private void OnEnable()
    {
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnGameplayActivePowerUp += ManualActive;
    }

    private void OnDisable()
    {
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnGameplayActivePowerUp -= ManualActive;
    }

    public void Set(float timeout)
    {
        this.timeout = timeout;
    }

    public void ViewPowerUp(bool enable)
    {
        canvasGP.alpha = (enable) ? 1f : 0f;
    }

    public void ManualActive()
    {
        if (!btnPressPowerUp.interactable)
        {
            return;
        }
        OnClick();
    }

    private void OnDestroy()
    {
        LeanTween.cancel(tweenTimeOut);
        LeanTween.cancel(tweenValueChange);
    }

    public void UpdateValue(int current, int limit)
    {
        //Debug.LogError($"Update PU: [{current}/{limit}]");
        if (current == limit)
        {
            btnPressPowerUp.image.raycastTarget = true;
            imgRadioBar.fillAmount = fillValue;
            btnPressPowerUp.interactable = true;
            animator.SetBool(ANIM_BOOL_AVAILABLE, true);
            LeanTween.cancel(tweenValueChange);
            imgHL.enabled = true;

        }
        else
        {
            btnPressPowerUp.image.raycastTarget = false;
            float next = current / (float)limit;
            float currentValue = imgRadioBar.fillAmount;
            //Debug.LogError($"fill to: {currentValue}-->{next}");
            tweenValueChange = LeanTween.value(currentValue, next, timeValueChange)
                .setOnUpdate(MoveProgressValue).uniqueId;
        }
    }


    private void MoveProgressValue(float promValue)
    {
        imgRadioBar.fillAmount = promValue;
    }


    private void OnClick()
    {
        btnPressPowerUp.image.raycastTarget = false;
        btnPressPowerUp.interactable = false;
        animator.SetBool(ANIM_BOOL_AVAILABLE, false);
        animator.SetBool(ANIM_BOOL_TIMEOUT, true);
        tweenTimeOut = LeanTween.value(fillValue, 0f, timeout)
            .setOnUpdate(MoveProgressValue)
            .setOnComplete(OnTimeOutEnd).uniqueId;
        OnClickEvt();
    }

    private void OnTimeOutEnd()
    {
        animator.SetBool(ANIM_BOOL_TIMEOUT, false);
        imgHL.enabled = false;

    }

}
