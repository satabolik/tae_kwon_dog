﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlanetGraphicHandler : MonoBehaviour
{
    [SerializeField]
    private Image imgBasePlanet;
    [SerializeField]
    private Image imgHighlightPlanet;
    [SerializeField]
    private Image imgPlanetFullMark;
    [SerializeField]
    private Image imgPlanetMarkPart1;
    [SerializeField]
    private Image imgPlanetMarkPart2;


    private void DisableMarks()
    {
        imgPlanetFullMark.enabled = false;
        imgPlanetMarkPart1.enabled = false;
        imgPlanetMarkPart2.enabled = false;
    }

    public void DrawPlanet(ColorsTemplatePlanet templateColor)
    {
        DisableMarks();
        imgBasePlanet.color = templateColor.colorBase;
        var alphaHL = imgHighlightPlanet.color.a;
        imgHighlightPlanet.color = templateColor.colorBase;
        GlobalSetAlpha.Set(imgHighlightPlanet, alphaHL);
        if (templateColor.type == TypePlanet.FullMark)
        {
            imgPlanetFullMark.enabled = true;
            imgPlanetFullMark.color = templateColor.colorMark1;
        }
        if (templateColor.type == TypePlanet.TwoMark)
        {
            imgPlanetMarkPart1.enabled = true;
            imgPlanetMarkPart2.enabled = true;
            imgPlanetMarkPart1.color = templateColor.colorMark1;
            imgPlanetMarkPart2.color = templateColor.colorMark2;
        }
    }

}
