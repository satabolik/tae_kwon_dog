﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolTablonMonos : MonoBehaviour
{

    [SerializeField]
    private List<ItemLevelNpc> listNpcLevel;

    private Dictionary<TypeLevel, List<PointItemNpc>> dicPoolNpc = new Dictionary<TypeLevel, List<PointItemNpc>>();


    public PointItemNpc GetNpcItemBoard(TypeLevel level)
    {
   
        if (dicPoolNpc.ContainsKey(level))
        {
            foreach (var npcPool in dicPoolNpc[level])
            {
                if (!npcPool.gameObject.activeSelf)
                    return npcPool;
            }    
        }
        var newNpc = CreateNewNpc(level);
        return newNpc;
    }

    private PointItemNpc CreateNewNpc(TypeLevel level)
    {
        var prefab = GetTemplate(level);
        var newNpc = Instantiate(prefab);
        if (!dicPoolNpc.ContainsKey(level))
            dicPoolNpc.Add(level, new List<PointItemNpc>());
        dicPoolNpc[level].Add(newNpc);
        newNpc.name = $"Npc_{level}_{dicPoolNpc[level].Count}";
        return newNpc;
    }

    private PointItemNpc GetTemplate(TypeLevel level)
    {
        foreach (var lvlPref in listNpcLevel)
        {
            if (lvlPref.type == level)
                return lvlPref.npcPrefab;
        }
        return null;
    }
}
[System.Serializable]
public struct ItemLevelNpc
{
    public TypeLevel type;
    public PointItemNpc npcPrefab;
}

public enum TypeLevel
{
    Top = 1,
    Mid = 2,
    Bot = 3
}
