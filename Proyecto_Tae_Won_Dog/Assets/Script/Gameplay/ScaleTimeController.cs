﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleTimeController : MonoBehaviour
{
    [SerializeField]
    private LeanTweenType tweenType;
    [SerializeField]
    private float timeResumeSpeed = 1f;
    [SerializeField]
    private float delayResumeTime = 0.25f;
    private float baseSpeed = 1f;
    private float originalSpeed;
    private int idTweenReanude;
    private void Awake()
    {
        originalSpeed = baseSpeed;
        ResetSpeed();
    }

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnGameOver += ResetSpeed;
    }
    private void OnDisable()
    {
        GlobaGameplaylEvents.OnGameOver -= ResetSpeed;
        if (LeanTween.tweensRunning > 0)
            if (LeanTween.isTweening(idTweenReanude))
                LeanTween.cancel(idTweenReanude);
    }

    public void AddSpeed(float percent)
    {
        baseSpeed += (baseSpeed * percent);
        Time.timeScale = baseSpeed;
    }

    public void AddTempSlowMotion(float percent, bool autoResume = true, float timeToMotion = 0f)
    {
        var targetSlowSpeed = baseSpeed - (baseSpeed * percent);
        if (timeToMotion == 0f)
        {
            Time.timeScale = targetSlowSpeed;
        }
        else
        {
            var currentTime = Time.timeScale;
            LeanTween.value(currentTime, targetSlowSpeed, timeToMotion)
                .setOnUpdate((v) => Time.timeScale = v);
        }

        if (LeanTween.isTweening(idTweenReanude))
            LeanTween.cancel(idTweenReanude);

        if (autoResume)
            ResumeCurrentTimeScale(delayResumeTime);
    }

    public void StopTimeScale()
    {
        if (LeanTween.isTweening(idTweenReanude))
            LeanTween.cancel(idTweenReanude);
        Time.timeScale = 0f;
    }

    public void ResumeCurrentTimeScale(float startDelay = 0f)
    {
      
        var currentTimeScale = Time.timeScale;

        idTweenReanude = LeanTween.value(currentTimeScale, baseSpeed, timeResumeSpeed)
            .setDelay(startDelay)
            .setIgnoreTimeScale(true)
            .setEase(tweenType)
            .setOnUpdate((v) => Time.timeScale = v)
            .uniqueId;
    }

    public void ResetSpeed()
    {
        Time.timeScale = originalSpeed;
    }
}