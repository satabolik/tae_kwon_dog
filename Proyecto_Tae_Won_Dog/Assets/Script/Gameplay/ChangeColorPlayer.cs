﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using RNG = UnityEngine.Random;
public class ChangeColorPlayer : MonoBehaviour
{

    [SerializeField]
    private float fadeTimeColor = 0.5f;

    private List<SpriteRenderer> topCloth = new List<SpriteRenderer>();
    private List<SpriteRenderer> botCloth = new List<SpriteRenderer>();
    private List<SpriteRenderer> belt = new List<SpriteRenderer>();

    public List<Color> RangeColors;
    private Dictionary<ClothType, int> dicClothIndex = new Dictionary<ClothType, int>();
    private bool completeColor;
    private Queue<Action> queueColorChange = new Queue<Action>();
    private Coroutine rutineLoadColorCharge;
    private void Awake()
    {
        var goBelt = GameObject.FindGameObjectsWithTag("Cinturon");
        var goTopCloth = GameObject.FindGameObjectsWithTag("TorsoSuperior");
        var goBotCloth = GameObject.FindGameObjectsWithTag("TorsoInferior");

        foreach (var go in goBelt)
            belt.Add(go.GetComponent<SpriteRenderer>());
        foreach (var go in goTopCloth)
            topCloth.Add(go.GetComponent<SpriteRenderer>());
        foreach (var go in goBotCloth)
            botCloth.Add(go.GetComponent<SpriteRenderer>());

        dicClothIndex.Add(ClothType.Belt, 0);
        dicClothIndex.Add(ClothType.Top, 0);
        dicClothIndex.Add(ClothType.Bot, 0);
    }

#if UNITY_EDITOR
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    ChangeNextColor();
        //}
    }
#endif
    public void AddNextColorChange()
    {
        queueColorChange.Enqueue(ChangeNextColor);
        if (rutineLoadColorCharge != null)
            StopCoroutine(rutineLoadColorCharge);

        rutineLoadColorCharge = StartCoroutine(ColorChangerRutine());
    }

    private void ChangeNextColor()
    {
        Color colorSelect;
        if (!completeColor)
        {
            var clothTypeList = GetNextCloth();
            foreach (var clothType in clothTypeList)
            {
                var index = dicClothIndex[clothType];
                colorSelect = RangeColors[index];
                ChangeColorCloth(clothType, colorSelect);
            }
        }
        else
        {
            ChangeColorCloth(ClothType.All, RandomColor());
        }

    }

    private IEnumerator ColorChangerRutine()
    {
        while (queueColorChange.Count > 0)
        {
            var colorChange = queueColorChange.Dequeue();
            colorChange();
            yield return null;
        }
    }

    private List<ClothType> GetNextCloth()
    {
        var colorCount = RangeColors.Count;
        var select = new List<ClothType>();

        foreach (var clothT in dicClothIndex.Keys.ToList())
        {
            var index = dicClothIndex[clothT];
            if (index < colorCount)
            {
                index++;
                select.Add(clothT);
                if (index == colorCount)
                {
                    dicClothIndex[clothT] = 0;
                    if (clothT == dicClothIndex.Last().Key)
                    {
                        completeColor = true;
                        select.Clear();
                        break;
                    }
                    continue;
                }
                dicClothIndex[clothT] = index;
                break;
            }
        }
        return select;
    }

    private List<SpriteRenderer> GetClothRender(ClothType type)
    {
        switch (type)
        {
            case ClothType.Belt:
                return belt;
            case ClothType.Top:
                return topCloth;
            case ClothType.Bot:
                return botCloth;
            case ClothType.All:
                return belt.Concat(topCloth.Concat(botCloth)).ToList();
        }
        return null;
    }

    private void ChangeColorCloth(ClothType type, Color targetColor)
    {
        var renders = GetClothRender(type);
        var startColor = renders[0].color;
        LeanTween.value(0, 1f, fadeTimeColor)
            .setOnUpdate((v) =>
            {
                foreach (var render in renders)
                    render.color = Color.Lerp(startColor, targetColor, v);
            });
    }


    Color RandomColor()
    {
        Color nuevoColor = new Color();
        nuevoColor.r = RNG.Range(0.1f, 0.9f);
        nuevoColor.b = RNG.Range(0.1f, 0.9f);
        nuevoColor.g = RNG.Range(0.1f, 0.9f);
        nuevoColor.a = 1;
        return nuevoColor;
    }
    private enum ClothType
    {
        Belt = 0,
        Top = 1,
        Bot = 2,
        All = 3
    }
}
