using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BreakableObjectsHandler : MonoBehaviour
{
    [TagSelector]
    [SerializeField]
    private string impactOtherTag = "";

    [TagSelector]
    [SerializeField]
    private string destroyThisTag = "";

    [SerializeField]
    private SC_ForceConfig forceConfig;
    [SerializeField]
    private int rewardScore = 1;
    [SerializeField]
    protected BreakableObjectController[] pieceBoard;
    [SerializeField]
    protected int damage = 1;
    [SerializeField]
    private GameObject efectoImpacto;
    [SerializeField]
    private float delayNextDamage = 0.25f;

    private bool wasHit = false;
    private ParticleSystem particulaImpacto;
    private WaitForSeconds waitSecondDamage;

    protected virtual void Start()
    {
        efectoImpacto = Instantiate(efectoImpacto, transform.position, Quaternion.identity);
        efectoImpacto.transform.parent = transform;
        particulaImpacto = efectoImpacto.GetComponent<ParticleSystem>();
        foreach (var bloq in pieceBoard)
        {
            bloq.SetForceConfig(forceConfig);
            bloq.OnGetHit += OnHitBreakableObject;
        }

        waitSecondDamage = new WaitForSeconds(delayNextDamage);
    }

    public void ResetAllBreakableStates()
    {
        wasHit = false;
        foreach (var bloq in pieceBoard)
            bloq.ResetState();
    }

    public void ApplyAllBreakableStates()
    {
        foreach (var bloq in pieceBoard)
            bloq.ApplyForce();
    }


#if UNITY_EDITOR
    protected virtual void Update()
    {
        if (Keyboard.current.rKey.IsPressed())
            foreach (var bloq in pieceBoard)
                bloq.ResetState();
    }
#endif

    protected virtual void OnHitBreakableObject(string tag)
    {
        if (GameplayManager.Instance.IsGameOver || WasHit())
            return;

        if (tag == destroyThisTag)
        {
            OnHitThis();
            return;
        }

        if (tag == impactOtherTag)
        {
            OnHitOther();
        }
    }

    protected virtual void OnHitThis()
    {
        wasHit = true;
        particulaImpacto.Play();
        SoundManager.Instance.PlaySfx(SfxType.HitPlank);
        GameplayManager.Instance.ProcessAttackPlank(rewardScore);
    }

    protected virtual void OnHitOther()
    {
        if (GameplayManager.Instance.IsSpecialModeActive)
            return;
        wasHit = true;
        StartCoroutine(ProcessAllDamage());
    }

    private IEnumerator ProcessAllDamage()
    {
        for (int i = 0; i < damage; i++)
        {
            GlobaGameplaylEvents.OnPlayerGetHit(1);
            yield return waitSecondDamage;
        }
    }

    protected virtual bool WasHit()
    {
        return wasHit;
    }
}
