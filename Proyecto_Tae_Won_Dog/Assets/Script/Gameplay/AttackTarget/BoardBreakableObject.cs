﻿using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;

public class BoardBreakableObject : BreakableObjectsHandler
{
    [SerializeField]
    private SpriteRenderer sprHighlight;
    private int tweenHL;
    private const float FADE_HL_TIME = 0.5f;

    public void EnableHighLight()
    {
        if (sprHighlight.enabled)
            return;
        sprHighlight.enabled = true;
        HL_FadeIn();
    }
    public void DisableHighLight()
    {
        if (!sprHighlight.enabled)
            return;
        if (LeanTween.isTweening(tweenHL))
            LeanTween.cancel(tweenHL);
        sprHighlight.enabled = false;
    }

    private void HL_FadeOut()
    {
        tweenHL = LeanTween.value(1f, 0f, FADE_HL_TIME)
            .setIgnoreTimeScale(true)
            .setOnUpdate(SetHighligh)
            .setOnComplete(HL_FadeIn)
            .uniqueId;
    }
    private void HL_FadeIn()
    {
        tweenHL = LeanTween.value(0f, 1f, FADE_HL_TIME)
            .setIgnoreTimeScale(true)
            .setOnUpdate(SetHighligh)
            .setOnComplete(HL_FadeOut)
            .uniqueId;
    }

    private void SetHighligh(float alphaValue)
    {
        if (sprHighlight)
        {
            GlobalSetAlpha.Set(sprHighlight, alphaValue);
        }
    }

    protected override void OnHitThis()
    {
        base.OnHitThis();
        GlobaGameplaylEvents.OnDestroyBoard();

    }
}
