﻿using UnityEngine;
using UnityEditor;
using System;

public class BreakableObjectController : MonoBehaviour
{
    [TagSelector]
    [SerializeField]
    private string targetTag = "";
    private bool wasHit = false;
    private Vector3 startPosition;
    private SC_ForceConfig forceConfig;
    private Rigidbody2D rigid2d;
    private Collider2D coll2d;
    public event Action<string> OnGetHit;
    

    void Awake()
    {
        rigid2d = GetComponent<Rigidbody2D>();
        coll2d = GetComponent<Collider2D>();
        startPosition = transform.localPosition;
    }

    public void SetForceConfig(SC_ForceConfig forceConfig) => this.forceConfig = forceConfig;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (OnGetHit != null)
            OnGetHit(other.tag);
        if (other.tag == targetTag && !wasHit)
        {
            ApplyForce();
        }
    }

    public void ApplyForce()
    {
        if (wasHit)
            return;
        wasHit = true;
        coll2d.isTrigger = false;
        rigid2d.isKinematic = false;
        var forceX = forceConfig.GetRngForceX();
        var forceY = forceConfig.GetRngForceY();
        rigid2d.AddRelativeForce(new Vector2(forceX, forceY)); 
    }

    public void ResetState()
    {
        if (!wasHit)
            return;

        rigid2d.velocity = Vector3.zero;
        rigid2d.angularVelocity = 0f;
        rigid2d.angularDrag = 0f;
        coll2d.isTrigger = true;
        rigid2d.isKinematic = true;
        wasHit = false;
        transform.localPosition = startPosition;
        transform.localRotation = Quaternion.identity;
    }
}
