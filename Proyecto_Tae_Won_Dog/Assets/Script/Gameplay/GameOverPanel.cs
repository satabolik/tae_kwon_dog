﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField]
    private GameObject goNewEffect;
    [SerializeField]
    private float fadePanel = 0.5f;
    [SerializeField]
    private TMP_Text txtScoreValue;
    [SerializeField]
    private TMP_Text txtHighScoreValue;
    [SerializeField]
    private TMP_Text txtScoreRankValue;
    [SerializeField]
    private Button btnRetry;
    [SerializeField]
    private Button btnHome;
    [SerializeField]
    private Button btnLeaderboard;

    private Canvas canvasPanel;
    private CanvasGroup canvasGp;
    private int previousHG = 0;
    // Start is called before the first frame update
    void Start()
    {
        btnRetry.onClick.AddListener(OnClickRetry);
        btnHome.onClick.AddListener(OnClickHome);
        btnLeaderboard.onClick.AddListener(OnClickLeaderboard);
        canvasPanel = GetComponent<Canvas>();
        canvasGp = GetComponent<CanvasGroup>();
        canvasGp.alpha = 0f;
        canvasPanel.enabled = false;
        goNewEffect.SetActive(false);

    }

    private void OnClickRetry()
    {
        btnRetry.interactable = false;
        SoundManager.Instance.PlaySfx(SfxType.PressButton, () => SceneManager.LoadScene(ScenesNames.GAMEPLAY));
    }
    private void OnClickHome()
    {
        btnHome.interactable = false;
        SoundManager.Instance.PlaySfx(SfxType.PressButton, () => SceneManager.LoadScene(ScenesNames.MAIN));
    }
    private void OnClickLeaderboard()
    {
        if (SocialManager.Instance != null)
        {
            SoundManager.Instance.PlaySfx(SfxType.PressButton, SocialManager.Instance.ShowLeaderboard);
        }
    }

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnGameOver += Show;
    }
    private void OnDisable()
    {
        GlobaGameplaylEvents.OnGameOver -= Show;
    }

    private void Show()
    {
        previousHG = SaveDataManager.Instance.GetHighScore();
        SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        ButtonLayoutManager.Instance.ActiveButtonLayoutPanel(LayoutKeyEnum.Gameover);

        EventSystem.current.SetSelectedGameObject(btnLeaderboard.gameObject);
        canvasPanel.enabled = true;
        var currentScore = GameplayManager.Instance.CurrentPoint;

        txtHighScoreValue.enabled = false;
        txtScoreValue.text = currentScore.ToString();

        canvasGp.interactable = false;
        LeanTween.value(0f, 1f, fadePanel)
            .setOnUpdate((v) => canvasGp.alpha = v)
            .setOnComplete(EndShowGameoverCanvas);

        if (SocialManager.Instance != null)
        {
            SocialManager.Instance.CheckAllScoresArchivement(currentScore, OnEndCheckArchivmentScore);
            SocialManager.Instance.UpdateScoreLeaderBoard(currentScore, ProcessUpdateRankText);
        }
    }

    private void EndShowGameoverCanvas()
    {
        canvasGp.interactable = true;
        GlobaGameplaylEvents.OnGameOverShowCompleted();
    }


    private void OnEndCheckArchivmentScore()
    {
        int maxHitCounterReached = GameplayManager.Instance.MaxHitCounterReached;
        SocialManager.Instance.CheckHitArchivement(maxHitCounterReached, OnEndCheckHitArchivement);
    }

    private void OnEndCheckHitArchivement()
    {
        int planksDestroy = GameplayManager.Instance.PlacksDestroyCount;
        SocialManager.Instance.CheckPlankDestroyArchivment(planksDestroy);
    }


    private void UpdateHighScore()
    {
        int currentScore = GameplayManager.Instance.CurrentPoint;
        int hgScore = previousHG;
        bool isNewHS = hgScore < currentScore;
        if (isNewHS)
        {
            hgScore = currentScore;
        }

        txtHighScoreValue.enabled = true;
        txtHighScoreValue.text = hgScore.ToString();
        goNewEffect.SetActive(isNewHS);
    }

    private void ProcessUpdateRankText()
    {
        SocialManager.Instance.LoadPlayerScore(UpdateRankTextAndHG);
    }
    private void UpdateRankTextAndHG(int rank)
    {
        UpdateHighScore();

        if (rank <= 0 || rank > 999)
        {
            txtScoreRankValue.text = string.Empty;
        }
        else
        {
            txtScoreRankValue.text = $"RANK #{rank}";
        }

    }
}
