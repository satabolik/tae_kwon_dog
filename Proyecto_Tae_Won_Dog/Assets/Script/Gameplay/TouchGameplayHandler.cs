﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class TouchGameplayHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler/*, ISubmitHandler*/
{
    public event Action OnPointerDownEvent;
    public event Action OnPointerUpEvent;
    public event Action OnPointerStayEvent;

    private bool isPointerDown;
    private bool isSubmitDown;

    private void Update()
    {
        if (isPointerDown && OnPointerStayEvent != null)
        {
            OnPointerStayEvent();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDown = true;
        if (OnPointerDownEvent != null)
            OnPointerDownEvent();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;

        if (OnPointerUpEvent != null)
            OnPointerUpEvent();
    }
}
