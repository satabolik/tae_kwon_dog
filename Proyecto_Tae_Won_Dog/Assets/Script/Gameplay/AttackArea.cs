﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackArea : MonoBehaviour
{
    private Collider2D areaCollider;
    private TrailRenderer trailRender;


    public void Active(bool value)
    {
        AreaCollider.enabled = value;
        TrailRender.enabled = value;
    }

    public Collider2D AreaCollider
    {
        get
        {
            if (areaCollider == null)
                areaCollider = GetComponent<Collider2D>();
            return areaCollider;
        }
    }
    public TrailRenderer TrailRender
    {
        get
        {
            if (trailRender == null)
                trailRender = GetComponent<TrailRenderer>();
            return trailRender;
        }
    }
}
