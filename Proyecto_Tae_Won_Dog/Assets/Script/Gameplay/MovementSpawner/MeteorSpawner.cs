using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;

public class MeteorSpawner : BreakableObjectsHandler
{
    [SerializeField]
    private float speedMin;

    [SerializeField]
    private float speedMax;

    [SerializeField]
    private float timePerSpawn;

    [SerializeField]
    [Range(0f, 1f)]
    private float chanceToSpawn;

    [SerializeField]
    private float incDownPerSpawnMin;

    [SerializeField]
    private float incDownPerSpawnMax;

    [SerializeField]
    private float percentScalePerSpawnMin;

    [SerializeField]
    private float percentScalePerSpawnMax;

    [SerializeField]
    private float maxScale;

    [SerializeField]
    private float MaxDownPosition;

    [SerializeField]
    private MeteorItemMovement meteorItemMovement;

    [SerializeField]
    private GameObject[] desactivesOnHit;

    private WaitForSeconds waitForSecondsRealtime;

    private float extendPosY;

    protected override void Start()
    {
        base.Start();
        meteorItemMovement.OnnDeactive += MeteorItemMovement_OnnDeactive;
        waitForSecondsRealtime = new WaitForSeconds(timePerSpawn);
        extendPosY = 0f;
        StartCoroutine(TrySpawnMeteorRutine());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void StartMove()
    {
        meteorItemMovement.gameObject.SetActive(true);
        ResetAllBreakableStates();

        float scaleSelect = Random.Range(percentScalePerSpawnMin, percentScalePerSpawnMax);

        var localScale = meteorItemMovement.transform.localScale;
        localScale.x += scaleSelect;
        float scale = Mathf.Min(localScale.x, maxScale);
        meteorItemMovement.transform.localScale = Vector3.one * scale;

        float incPosSelect = Random.Range(incDownPerSpawnMin, incDownPerSpawnMax);
        var currentPosition = meteorItemMovement.transform.localPosition;

        extendPosY -= incPosSelect;
        float limitPosition = Mathf.Max(extendPosY, MaxDownPosition);
        currentPosition.y = Random.Range(limitPosition, 0f);
        currentPosition.x = 0f;
        meteorItemMovement.transform.localPosition = currentPosition;

        float speedSelect = Random.Range(speedMin, speedMax);
        meteorItemMovement.StartMove(speedSelect);

        foreach (var go in desactivesOnHit)
        {
            go.SetActive(true);
        }
    }

    private IEnumerator TrySpawnMeteorRutine()
    {
        while (true)
        {
            yield return waitForSecondsRealtime;
            
            if (Random.value <= chanceToSpawn)
            {
                StartMove();
                yield break;
            }
        }
    }

    private void MeteorItemMovement_OnnDeactive()
    {
        StartCoroutine(TrySpawnMeteorRutine());
    }

    protected override void OnHitThis()
    {
        base.OnHitThis();
        ApplyAllBreakableStates();
        foreach(var go in desactivesOnHit)
        {
            go.SetActive(false);
        }
    }
}
