using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorItemMovement : ItemMovementBase
{
    public event Action OnnDeactive;

    protected override void OnDeactiveTagCollision()
    {
        gameObject.SetActive(false);
        OnnDeactive?.Invoke();
    }

    protected override void OnInteractTagCollision()
    {

    }
}
