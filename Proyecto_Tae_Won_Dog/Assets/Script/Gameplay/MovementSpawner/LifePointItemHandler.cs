﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifePointItemHandler : ItemMovementBase
{
    protected override void OnDeactiveTagCollision()
    {
        gameObject.SetActive(false);
    }

    protected override void OnInteractTagCollision()
    {
        if (!GameplayManager.Instance.IsInMaxLife())
        {
            GameplayManager.Instance.AddLifePlayer();
            gameObject.SetActive(false);
        }
    }
}