﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcItemSpawner : MonoBehaviour
{
    [SerializeField]
    private float speedMove = 0.1f;
    [SerializeField]
    private PoolTablonMonos pool;
    [SerializeField]
    private float timeSpawnMin = 2f;
    [SerializeField]
    private float timeSpawnMax = 2f;
    [SerializeField]
    private LevelPercentSpawn[] levelsPercentSpawn;

    private bool manualSpawn;
    private List<TypeLevel> rngLevel;
    private Coroutine rutineSpawnRuning;

    private void Awake()
    {
        rngLevel = System.Enum.GetValues(typeof(TypeLevel)).Cast<TypeLevel>().ToList();
        levelsPercentSpawn = levelsPercentSpawn.OrderBy((l) => l.percent).ToArray();
    }

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnStartRun += StartSpawn;
        GlobaGameplaylEvents.OnGameOver += StopSpawn;
    }
    private void OnDisable()
    {
        GlobaGameplaylEvents.OnStartRun -= StartSpawn;
        GlobaGameplaylEvents.OnGameOver -= StopSpawn;
    }

    private void StartSpawn()
    {
        if (manualSpawn)
            return;
        StopSpawn();
        rutineSpawnRuning = StartCoroutine(SpawnNpcRutine());
    }

    private void StopSpawn()
    {
        if (rutineSpawnRuning != null)
            StopCoroutine(rutineSpawnRuning);
    }

    private IEnumerator SpawnNpcRutine()
    {
        while (true)
        {
            var lvl = GetNextLevel();
            SpawnNpc(lvl);
            var secondRng = Random.Range(timeSpawnMin, timeSpawnMax);
            yield return new WaitForSeconds(secondRng);

        }
    }

    public void SpawnNpc(TypeLevel level, bool enableHL = false)
    {
        var npc = pool.GetNpcItemBoard(level);
        npc.gameObject.SetActive(true);
        npc.BoardCtrll.ResetAllBreakableStates();
        npc.transform.position = transform.position;
        npc.StartMove(speedMove);
        if (enableHL)
            npc.BoardCtrll.EnableHighLight();
    }

    public bool ManualSpawn { get => manualSpawn; set => manualSpawn = value; }


    private TypeLevel GetNextLevel()
    {
        var rng = Random.value;
        foreach (var levelPer in levelsPercentSpawn)
        {
            if (rng <= levelPer.percent)
                return levelPer.level;
        }
        return TypeLevel.Bot;
    }
    [System.Serializable]
    public struct LevelPercentSpawn
    {
        public TypeLevel level;
        [Range(0.0f, 1.0f)]
        public float percent;
    }

}
