﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointItemNpc : MonoBehaviour
{
    [SerializeField]
    private BoardBreakableObject boardCtrll;
    private Coroutine corutineMovement;

    public void StartMove(float speed)
    {
        corutineMovement = StartCoroutine(MovementRutine(speed));
    }
    private void OnDisable()
    {
        if (corutineMovement != null)
            StopCoroutine(corutineMovement);
    }

    private IEnumerator MovementRutine(float speed)
    {
        while (true)
        {
            var pos = transform.position;
            pos.x += speed * Time.deltaTime;
            transform.position = pos;
            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Destructor")
        {
            gameObject.SetActive(false);
        }
    }

    public BoardBreakableObject BoardCtrll { get => boardCtrll; }
}
