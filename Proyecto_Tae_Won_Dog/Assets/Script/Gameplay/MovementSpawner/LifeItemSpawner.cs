﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public class LifeItemSpawner : MonoBehaviour
{
    [SerializeField]
    private float speedX = 10f;
    [SerializeField]
    private float timeZigZag = 0.5f;
    [SerializeField]
    private LifePointItemHandler lifeItem;
    [SerializeField]
    private LeanTweenType tweenMove;
    [SerializeField]
    private Transform topPosition;
    [SerializeField]
    private Transform botPosition;

    private bool isMovingTop;
    //[SerializeField]
    //private Transform currentTargetPosition;
    [SerializeField]
    private int hitRequiredSpawnItem = 10;
    [SerializeField]
    private float probabilityStartAppear = 1f;
    [SerializeField]
    private float probabilityMinAppear = 0.1f;
    [SerializeField]
    private float probabilityChangeRest = 0.2f;

    private int idTweenZigZag;
    private int currentHitRequired;
    private float currentProbability;
    // Start is called before the first frame update
    void Start()
    {
        currentHitRequired = hitRequiredSpawnItem;
        currentProbability = probabilityStartAppear;
        //lifeItem.gameObject.SetActive(false);
        GlobaGameplaylEvents.OnUpdateHitCount += OnUpdateHitCounter;
    }
    private void OnDestroy()
    {
        GlobaGameplaylEvents.OnUpdateHitCount -= OnUpdateHitCounter;
        if (LeanTween.isTweening(idTweenZigZag))
            LeanTween.cancel(idTweenZigZag);
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Keyboard.current.lKey.isPressed)
        {
            OnUpdateHitCounter(currentHitRequired);
        }
    }
#endif

    private void OnUpdateHitCounter(int hitCounter)
    {
        if (hitCounter == 0)
        {
            currentHitRequired = hitRequiredSpawnItem;
            return;
        }

        if (hitCounter >= currentHitRequired)
        {
            currentHitRequired += hitRequiredSpawnItem;
            if (Random.value <= currentProbability)
            {
                StartCoroutine(CorutineInvokeItemLife());
                var nextProb = currentProbability - probabilityChangeRest;
                currentProbability = Mathf.Max(probabilityMinAppear, nextProb);
            }
        }
    }


    private IEnumerator CorutineInvokeItemLife()
    {
        while (lifeItem.gameObject.activeSelf)
        {        
            yield return null;
        }
        lifeItem.gameObject.SetActive(true);
        lifeItem.StartMove(speedX);
        lifeItem.gameObject.transform.localPosition = Vector3.zero;
        if (LeanTween.isTweening(idTweenZigZag))
            LeanTween.cancel(idTweenZigZag);
        MoveZigZag();
    }

    private void MoveZigZag()
    {
        var targetY = (isMovingTop) ? botPosition.localPosition.y : topPosition.localPosition.y;
        idTweenZigZag = LeanTween.moveLocalY(lifeItem.gameObject, targetY, timeZigZag)
         .setEase(tweenMove)
         .setOnComplete(() =>
         {
             isMovingTop = !isMovingTop;
             MoveZigZag();
         }).uniqueId;
    }
}
