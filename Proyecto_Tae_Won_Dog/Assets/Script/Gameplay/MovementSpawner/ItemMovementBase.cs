using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemMovementBase : MonoBehaviour
{
    [SerializeField]
    [TagSelector]
    private string interactTag;

    [SerializeField]
    [TagSelector]
    private string deactiveTag;

    [SerializeField]
    private float moveDownPercent = 0f;

    private Coroutine corutineMoveItem;
    private Vector3 originalPos;

    public void StartMove(float speed)
    {
        corutineMoveItem = StartCoroutine(MoveItemRutine(speed));
        originalPos = transform.position;
    }

    private IEnumerator MoveItemRutine(float speed)
    {
        while (true)
        {
            transform.position += Vector3.right * (speed * Time.deltaTime);
            transform.position += Vector3.down * (Math.Abs(speed) * Time.deltaTime) * moveDownPercent;
            yield return null;
        }
    }

    protected virtual void OnDisable()
    {
        if (corutineMoveItem != null)
            StopCoroutine(corutineMoveItem);
        transform.position = originalPos;
    }

    protected abstract void OnInteractTagCollision();

    protected abstract void OnDeactiveTagCollision();

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == interactTag)
        {
            OnInteractTagCollision();
        }
        if (collision.tag == deactiveTag)
        {
            OnDeactiveTagCollision();
        }
    }
}
