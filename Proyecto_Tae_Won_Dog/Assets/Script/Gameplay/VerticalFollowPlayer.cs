using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalFollowPlayer : MonoBehaviour
{
    [SerializeField]
    private Transform followPlayer;
    [SerializeField]
    private float startfollowY;
    [SerializeField]
    private float maxPos;
    private float minPos;

    private void Start()
    {
        minPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {

        transform.position = new Vector3(
            transform.position.x,
            Math.Clamp(followPlayer.position.y + startfollowY, minPos, maxPos),
            transform.position.z);
    }
}
