﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtackAnimEvent : MonoBehaviour
{
    [SerializeField]
    private AttackArea leftAttackRadio;
    [SerializeField]
    private AttackArea rightAttackRadio;

    private void Awake()
    {
        leftAttackRadio.Active(false);
        rightAttackRadio.Active(false);
    }

    private void StartLeftAttack()
    {
        leftAttackRadio.Active(true);
    }
    private void EndLeftAttack()
    {
        leftAttackRadio.Active(false);
    }
    private void StartRightAttack()
    {
        rightAttackRadio.Active(true);
    }
    private void EndRightAttack()
    {
        rightAttackRadio.Active(false);
    }
}
