﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ScrollGroundTileHandler : MonoBehaviour
{
    [SerializeField]
    private Tilemap tileMap;
    [SerializeField]
    private Vector3Int positionGroundAddMin;
    [SerializeField]
    private Vector3Int positionGroundAddMax;
    [SerializeField]
    private TileBase[] groundTiles;

    [SerializeField]
    private float speedScroll = 1f;
    [SerializeField]
    private BindingCollider2d colliderToReset;
    [SerializeField]
    private string tagColliderReset;
    [SerializeField]
    private Vector3 resetPosition;

    // Start is called before the first frame update
    void Start()
    {
        GlobaGameplaylEvents.OnStartRun += StartTileGenerator;
        GlobaGameplaylEvents.OnGameOverShowCompleted += StopAllCoroutines;
        Collider2D tilemapCollider = tileMap.GetComponent<Collider2D>();
        colliderToReset.OnCollisionEnter += ColliderToReset_OnCollisionEnter;

    }


    private void OnDisable()
    {
        StopAllCoroutines();
        GlobaGameplaylEvents.OnStartRun -= StartTileGenerator;
        GlobaGameplaylEvents.OnGameOverShowCompleted -= StopAllCoroutines;
        colliderToReset.OnCollisionEnter -= ColliderToReset_OnCollisionEnter;

    }
    private void ColliderToReset_OnCollisionEnter(Collider2D obj)
    {

        if (obj.tag == tagColliderReset)
        {
            tileMap.transform.localPosition = resetPosition;
        }
    }

    private void StartTileGenerator()
    {
        StartCoroutine(ScrollingTileRutine());
    }


    private void AddTiles(uint count)
    {
        if (count <= 0)
            return;
        Vector3Int positionAdd = default;
        for (int i = 0; i < count; i++)
        {
            positionAdd.x = Random.Range(positionGroundAddMin.x, positionGroundAddMax.x);
            positionAdd.y = Random.Range(positionGroundAddMin.y, positionGroundAddMax.y);
            positionAdd.z = Random.Range(positionGroundAddMin.z, positionGroundAddMax.z);

            tileMap.SetTile(positionAdd, groundTiles[Random.Range(0, groundTiles.Length)]);
            tileMap.SetColor(positionAdd, Color.red);
        }
    }


    private IEnumerator ScrollingTileRutine()
    {
        while (true)
        {
            tileMap.transform.position += (Vector3.left * Time.deltaTime * speedScroll);
            yield return null;
        }
    }
}
