﻿using UnityEngine;
using System.Collections;

public class CambiarResolucion : MonoBehaviour {
	private int[] anchos;
	private int[] altos;
	private static int anchoMax = 0;
	private static int altoMax = 0;
	private static int indiceDeResolucion = 0;
	public TextMesh textoResolucionMaxima;
	void Awake(){
		if (anchoMax == 0) {
			anchoMax = Screen.width;
			altoMax = Screen.height;
		}
		anchos = new int[]{128,256,384,512,640,800,1024,1152,1280,1365,1408,1536,1980};
		altos =  new int[]{72,144,216,288,360,450,576,648,720,768,792,864,1080};
		if (textoResolucionMaxima!= null){
			if (indiceDeResolucion>0){
				textoResolucionMaxima.text = anchos[indiceDeResolucion-1].ToString() + " X " + altos[indiceDeResolucion-1].ToString ();
			}
			else{
				textoResolucionMaxima.text = anchoMax.ToString() + " X " + altoMax.ToString();
			}
		}
	}
	void OnMouseDown() {
		bool seCambia = true;
		
		if (anchos [indiceDeResolucion] <= anchoMax) {
			Screen.SetResolution(anchos[indiceDeResolucion],altos[indiceDeResolucion],true);
			if (textoResolucionMaxima!= null)textoResolucionMaxima.text = anchos[indiceDeResolucion].ToString() + " X " + altos[indiceDeResolucion].ToString ();
		}
		else{
			Screen.SetResolution(anchoMax,altoMax,true);
			if (textoResolucionMaxima!= null)textoResolucionMaxima.text = anchoMax.ToString() + " X " + altoMax.ToString ();
			seCambia = false;
		}
		
		indiceDeResolucion = (seCambia) ? (indiceDeResolucion + 1) : 0;
	}
}
