﻿using UnityEngine;
using System.Collections;

public class GatilladorParticula : MonoBehaviour {
	private bool activarParticula = false;
	private ParticleSystem particula;
	// Use this for initialization
	void Start () {
		particula = gameObject.GetComponent<ParticleSystem>();
		particula.Stop();
	}
	
	// Update is called once per frame
	void Update () {
		if (activarParticula){
			particula.Play ();
			activarParticula = false;
		}
		/*else{
			particula.Stop();
		}*/
	}

	public void ActivarSistemaParticula(bool activar){
		activarParticula = activar;
	}
}
