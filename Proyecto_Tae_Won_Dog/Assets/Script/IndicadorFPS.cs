﻿using UnityEngine;
using System.Collections;

public class IndicadorFPS : MonoBehaviour {
	private TextMesh texto;
	// Use this for initialization
	void Start () {
		texto = gameObject.GetComponent<TextMesh>();
		Invoke ("ActualizarTexto",0.1f);
	}

	void ActualizarTexto(){
		texto.text = (1.0f / Time.deltaTime).ToString();
		Invoke ("ActualizarTexto",0.25f);
	}
}
