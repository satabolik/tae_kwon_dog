﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CambiarColorRandomFondo : MonoBehaviour
{

    [SerializeField]
    private float delayChange = 3f;
    [SerializeField]
    private float delayFade = 0.5f;
    private WaitForSeconds second;
    private Image imgFondo;
    private Coroutine rutinaColor;
    private LTDescr tweenColor;
    void Start()
    {

        second = new WaitForSeconds(delayChange);
        imgFondo = GetComponent<Image>();
        imgFondo.color = ColorRandom();
        rutinaColor = StartCoroutine(ColorChange());
    }

    private void OnDestroy()
    {
        if (rutinaColor != null)
        {
            StopCoroutine(rutinaColor);
            if (tweenColor != null)
                LeanTween.cancel(tweenColor.uniqueId);
        }
    }


    private IEnumerator ColorChange()
    {
        while (true)
        {
            yield return second;
            var currentColor = imgFondo.color;
            var nextColor = ColorRandom();
            tweenColor = LeanTween.value(0f, 1f, delayFade)
                .setOnUpdate((v) =>
                {
                    imgFondo.color = Color.Lerp(currentColor, nextColor, v);
                });
        }
    }

    private Color ColorRandom()
    {
        Color nuevoColor = new Color();
        nuevoColor.r = Random.Range(0.3f, 0.9f);
        nuevoColor.b = Random.Range(0.3f, 0.9f);
        nuevoColor.g = Random.Range(0.3f, 0.9f);
        nuevoColor.a = 1;
        return nuevoColor;
    }
}
