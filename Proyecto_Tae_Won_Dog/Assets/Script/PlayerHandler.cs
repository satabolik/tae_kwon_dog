﻿using UnityEngine;
using System.Collections;
using System;
using EZCameraShake;
public class PlayerHandler : MonoBehaviour
{

    [SerializeField]
    private TouchGameplayHandler touchHandler;
    [SerializeField]
    private float MinJumpForce = 1.4f;
    [SerializeField]
    private float maxJumpForce = 30f;
    [SerializeField]
    private float percentExtraJumpToMax = 1.25f;
    [SerializeField]
    private float delayEntreAtaque = 0.23f;
    [SerializeField]
    private float frenoCaidaPorHit = -0.25f;
    [SerializeField]
    private float tiempoRecargaImpulso = 3f;
    [SerializeField]
    private Transform comprobadorSuelo;
    [SerializeField]
    private LayerMask mascaraSuelo;
    [SerializeField]
    private ParticleSystem impactParticleEffect;
    [SerializeField]
    private ParticleSystem activeSpecialParticleEffect;

    // private bool especial = false;
    private float currentJumpForce = 1.4f;
    private float radioComprobadorSuelo = 0.65f;
    private Animator animator;
    private bool onFloor = true;
    private bool isRunning = false;
    private bool jumpAttack = false;
    private bool isLeftAttack = true;
    private bool enDelayAtaque = false;
    private Rigidbody2D rigidBody;
    private Coroutine corutineDelayAttack;
    private WaitForSeconds secondDelayAttack;


    void Awake()
    {
       
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
        animator.SetLayerWeight(1, 1f);
        secondDelayAttack = new WaitForSeconds(delayEntreAtaque);
        //vidaMax = vida;
        currentJumpForce = MinJumpForce;
    }

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnPlayerGetHit += PlayerGetHit;
        GlobaGameplaylEvents.OnActiveSpecial += OnSpecialActivated;
        GlobaGameplaylEvents.OnSpecialOver += OnSpecialOver;
        GlobaGameplaylEvents.OnUpdateHitCount += ImpulseJumpActive;
        touchHandler.OnPointerDownEvent += OnTouchDown;
        touchHandler.OnPointerUpEvent += OnTouchUp;
        touchHandler.OnPointerStayEvent += OnTouching;

        SettingConfigManager.Instance.OnGameplayActionStart += OnTouchDown;
        SettingConfigManager.Instance.OnGameplayActionEnd += OnTouchUp;
        SettingConfigManager.Instance.OnGameplayActionStay += OnTouching;

    }
    private void OnDisable()
    {
        GlobaGameplaylEvents.OnPlayerGetHit -= PlayerGetHit;
        GlobaGameplaylEvents.OnActiveSpecial -= OnSpecialActivated;
        GlobaGameplaylEvents.OnUpdateHitCount -= ImpulseJumpActive;
        GlobaGameplaylEvents.OnSpecialOver -= OnSpecialOver;

        touchHandler.OnPointerDownEvent -= OnTouchDown;
        touchHandler.OnPointerUpEvent -= OnTouchUp;
        touchHandler.OnPointerStayEvent -= OnTouching;

        if (SettingConfigManager.Instance != null) 
        {     
            SettingConfigManager.Instance.OnGameplayActionStart -= OnTouchDown;
            SettingConfigManager.Instance.OnGameplayActionEnd -= OnTouchUp;
            SettingConfigManager.Instance.OnGameplayActionStay -= OnTouching;
        }
    }

    private void OnTouching()
    {
        if (onFloor)
            IncrementandoImpulsoSalto();
    }

    private void OnTouchDown()
    {
        if (GameplayManager.Instance.IsGameOver)
            return;
        if (!onFloor)
            PlayerAttack();
    }

    private void OnTouchUp()
    {
        if (GameplayManager.Instance.IsGameOver)
            return;
        if (onFloor)
        {
            if (!isRunning)
                StartRuning();
            else
                PlayerJump();
        }

    }

    private void OnSpecialActivated()
    {
        activeSpecialParticleEffect.Play();
        animator.SetBool("ataqueEspecial", true);
    }

    private void OnSpecialOver()
    {
        activeSpecialParticleEffect.Stop();
    }

    void FixedUpdate()
    {
        var checkOnFloor = Physics2D.OverlapCircle(comprobadorSuelo.position, radioComprobadorSuelo, mascaraSuelo);
        if (checkOnFloor != onFloor)
        {
            onFloor = checkOnFloor;
            if (onFloor)
            {
                GlobaGameplaylEvents.OnPlayerFallOnFloor();
                ReiniciarValores();

            }
        }
        ActualizarEstadosDeAnimacion();
    }

    /// <summary>
    /// Actualiza los estados del animator del personaje.
    /// </summary>
    private void ActualizarEstadosDeAnimacion()
    {
        animator.SetFloat("velocidadX", (isRunning) ? 1f : 0f);
        animator.SetBool("enSuelo", onFloor);
        animator.SetBool("cambiarAtaque1", isLeftAttack);
        animator.SetBool("cambiarAtaque2", !isLeftAttack);
        animator.SetBool("bajando", (rigidBody.velocity.y < 0 && (!onFloor)));
        animator.SetBool("ataqueEspecial", IsSpecialActivate());
        animator.SetFloat("forceJump", currentJumpForce / maxJumpForce);
    }

    private void IncrementandoImpulsoSalto()
    {
        if (!GameplayManager.Instance.EnableJump)
            return;
        if (currentJumpForce < maxJumpForce)
            currentJumpForce += (Time.deltaTime / tiempoRecargaImpulso) * maxJumpForce;
        if (currentJumpForce > maxJumpForce)
            currentJumpForce = maxJumpForce;
    }

    private void StartRuning()
    {
        if (!GameplayManager.Instance.EnableRuning)
            return;
        isRunning = true;
        GlobaGameplaylEvents.OnStartRun();
    }

    private void PlayerJump()
    {
        if (!GameplayManager.Instance.EnableJump)
            return;
        rigidBody.velocity = new Vector2(0f, currentJumpForce);
        GlobaGameplaylEvents.OnPlayerJump(currentJumpForce);
        currentJumpForce = MinJumpForce;
    }

    private void PlayerAttack()
    {
        if (!GameplayManager.Instance.EnableAttack)
            return;
        if (enDelayAtaque)
            return;

        enDelayAtaque = true;
        animator.ResetTrigger("atacando");
        animator.SetTrigger("atacando");
        SoundManager.Instance.PlaySfx(SfxType.playerAtack);
        //StopAllCoroutines();
        if (jumpAttack)
        {
            jumpAttack = false;
            float nuevoImpulso = (percentExtraJumpToMax * maxJumpForce);
            rigidBody.velocity = new Vector2(0f, nuevoImpulso);
        }
        if (IsSpecialActivate())
        {
            isLeftAttack = false;
            jumpAttack = true;
        }
        else
            isLeftAttack = !isLeftAttack;
        corutineDelayAttack = StartCoroutine(TerminarEspera());
        GlobaGameplaylEvents.OnPlayerAttack();
    }
    /// <summary>
    /// Metodo que desactiva un bool en un determinado tiempo, para 
    ///  el caso de ataques consecutivos.
    /// </summary>
    /// <returns>The espera.</returns>
    private IEnumerator TerminarEspera()
    {
        yield return secondDelayAttack;
        enDelayAtaque = false;
    }

    /// <summary>
    /// Reiniciar los estados de valores activos.
    /// </summary>
    private void ReiniciarValores()
    {


        if (jumpAttack)
        {
            jumpAttack = false;
        }
        if (isLeftAttack)
        {
            isLeftAttack = true;
        }
    }



    /// <summary>
    /// Metodo que resta vida al jugador o define la muerte de este.
    /// </summary>
    /// <param name="notificacion">Notificacion con el dato de danio a restar.</param>
    public void PlayerGetHit(int hitDamage)
    {
        if (GameplayManager.Instance.IsGameOver)
            return;
        if (!GameplayManager.Instance.EnableDamage)
            return;

        SoundManager.Instance.PlaySfx(SfxType.HitPlayer);
        impactParticleEffect.Play();
        animator.SetTrigger("golpeado");

    }

    /// <summary>
    /// Metodo que incrementa el hit ().
    /// </summary>
    private void ImpulseJumpActive(int hit)
    {
        jumpAttack = true;
        if (rigidBody.velocity.y < 0f)
        {
            rigidBody.velocity = new Vector2(0f, (rigidBody.velocity.y * frenoCaidaPorHit));
        }
    }

    public bool IsSpecialActivate()
    {

        return GameplayManager.Instance.IsSpecialModeActive;
    }


}