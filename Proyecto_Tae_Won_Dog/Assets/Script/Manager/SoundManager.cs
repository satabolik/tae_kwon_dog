﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : Singleton<SoundManager>
{
    [SerializeField]
    private SC_SoundConfig soundConfig;
    [SerializeField]
    private AudioSource sfxSource;
    [SerializeField]
    private AudioSource musicSource;
    private float volumePreset;

    private void Start()
    {
        volumePreset = musicSource.volume;
        PlayMusic(SaveDataManager.Instance.IsOnMusic);
    }

    public void PlaySfx(SfxType type)
    {
        PlaySfx(type, null);
    }

    public void PlaySfx(SfxType type, System.Action onPostSfx)
    {
        if (!SaveDataManager.Instance.IsOnSound)
        {
            onPostSfx?.Invoke();
            return;
        }
        var sfxDat = soundConfig.GetSoundDat(type);
        if (sfxDat.clips.Length == 0)
        {
            onPostSfx?.Invoke();
            return;
        }
        var clip = sfxDat.clips[Random.Range(0, sfxDat.clips.Length)];
        if (onPostSfx != null)
        {
            StartCoroutine(PostSfxCorutine(clip.length * 0.6f, onPostSfx));
        }

        sfxSource.PlayOneShot(clip);
    }

    private IEnumerator PostSfxCorutine(float delay, System.Action callback)
    {
        yield return new WaitForSeconds(delay);
        callback();
    }

    public void PlayMusic(bool play)
    {
        musicSource.volume = (play) ? volumePreset : 0f;
    }
}
