﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem.XInput;
using UnityEngine.InputSystem.DualShock;

public class SettingConfigManager : Singleton<SettingConfigManager>
{
    [SerializeField]
    private GameMode currentMode;

    [SerializeField]
    private PlayerInput playerInput;

    public event Action OnUICancel;
    public event Action OnUISubmit;
    public event Action OnUIOpenSetting;
    public event Action<Vector2> OnUIGamepadScroll;

    public event Action OnGameplayActionStart;
    public event Action OnGameplayActionStay;
    public event Action OnGameplayActionEnd;
    public event Action OnGameplayOpenSetting;
    public event Action OnGameplayActivePowerUp;

    public event Action<DeviceType> OnDeviceChanged;
    public event Action<DeviceType> OnActiveDeviceDisconnected;


    private Coroutine actionStayRutine;
    private Coroutine scrollStayRutine;

    private DeviceType currentActiveDevice = DeviceType.none;
    private InputMode currentInputMode = InputMode.UI;


    private void TryUpdateDeviceChanged(InputDevice device)
    {
        if (TryUpdateDevice<XInputController>(device))
        {
            Cursor.visible = false;
        }
        if (TryUpdateDevice<DualShockGamepad>(device))
        {
            Cursor.visible = false;
        }
        if (TryUpdateDevice<Keyboard>(device)) return;
    }

    private bool TryUpdateDevice<T>(InputDevice device) where T : InputDevice
    {
        var deviceType = GetValidType(device);
        if (deviceType == DeviceType.none || deviceType == currentActiveDevice)
        {
            return false;
        }

        currentActiveDevice = deviceType;
        if (OnDeviceChanged != null)
        {
            OnDeviceChanged(currentActiveDevice);
        }

        return true;
    }


    private DeviceType GetValidType(InputDevice device)
    {
        if (device == null)
            return DeviceType.none;
        if (device is XInputController)
            return DeviceType.XboxGamepad;
        if (device is DualShockGamepad)
            return DeviceType.playGamepad;
        if ((device is Keyboard) || (device is Mouse))
            return DeviceType.keyboardMouse;
        return DeviceType.none;
    }

    public void SetInputMode(InputMode mode)
    {
        currentInputMode = mode;
        if (mode == InputMode.UI)
        {
            playerInput.actions.FindActionMap(InputMode.UI.ToString()).Enable();
            playerInput.actions.FindActionMap(InputMode.Gameplay.ToString()).Disable();
        }
        if (mode == InputMode.Gameplay)
        {
            playerInput.actions.FindActionMap(InputMode.UI.ToString()).Enable();
            playerInput.actions.FindActionMap(InputMode.Gameplay.ToString()).Enable();
        }
    }

    public void OnDeviceLost(PlayerInput playerInput)
    {
        foreach (var device in playerInput.devices)
        {
            if (GetValidType(device) == currentActiveDevice && !device.added)
            {
                Debug.Log($" active OnDeviceLost : {currentActiveDevice}");
                OnActiveDeviceDisconnected?.Invoke(currentActiveDevice);
                return;
            }
        }
    }

    public void OnPointer_performed(InputAction.CallbackContext context)
    {
        if (TryUpdateDevice<Mouse>(context.control.device))
        {
            Cursor.visible = true;
        }

    }

    public void OnClick_performed(InputAction.CallbackContext context)
    {
        if (TryUpdateDevice<Mouse>(context.control.device))
        {
            Cursor.visible = true;
        }
    }

    public void OnNavigate_performed(InputAction.CallbackContext context)
    {
        TryUpdateDeviceChanged(context.control.device);
    }

    public void OnCancel_performed(InputAction.CallbackContext context)
    {
        if (OnUICancel != null && context.performed)
        {
            OnUICancel();
        }

        TryUpdateDeviceChanged(context.control.device);
    }

    public void OnGamepadScroll_performed(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            scrollStayRutine = StartCoroutine(UpdateScrollPressing(context.action));
            TryUpdateDeviceChanged(context.control.device);
        }
        if (context.canceled && scrollStayRutine != null) 
        {
            StopCoroutine(scrollStayRutine);
        }
    }

    public void OnSubmit_performed(InputAction.CallbackContext context)
    {
        if (OnUISubmit != null && context.performed)
        {
            OnUISubmit();
        }

        TryUpdateDeviceChanged(context.control.device);
    }

    public void OnGameplayAction__performed(InputAction.CallbackContext context)
    {
        if (OnGameplayActionStart != null && context.started)
        {
            OnGameplayActionStart();
            actionStayRutine = StartCoroutine(UpdateActionStay());
        }
        if (OnGameplayActionEnd != null && context.canceled)
        {
            if (actionStayRutine != null)
            {
                StopCoroutine(actionStayRutine);
            }

            OnGameplayActionEnd();
        }

        TryUpdateDeviceChanged(context.control.device);
    }

    private IEnumerator UpdateActionStay()
    {
        while (OnGameplayActionStay != null) 
        {
            OnGameplayActionStay();
            yield return null;
        }
    }

    private IEnumerator UpdateScrollPressing(InputAction action)
    {
        while (OnUIGamepadScroll != null)
        {
            Vector2 vector2 = action.ReadValue<Vector2>();
            if (vector2 == Vector2.zero)
            {
                break;
            }
            OnUIGamepadScroll(vector2);
            yield return null;
        }
    }


    public void OnGameplayOpenSetting__performed(InputAction.CallbackContext context)
    {
        if (OnGameplayOpenSetting != null && context.performed)
        {
            OnGameplayOpenSetting();
        }
    }

    public void OnUIOpenSetting__performed(InputAction.CallbackContext context)
    {
        if (OnUIOpenSetting != null && context.performed)
        {
            OnUIOpenSetting();
        }
    }

    public void OnGameplayActivePowerUp__performed(InputAction.CallbackContext context)
    {
        if (OnGameplayActivePowerUp != null && context.performed)
        {
            OnGameplayActivePowerUp();
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneUnloaded -= SceneManager_sceneUnloaded;
    }

    private void SceneManager_sceneUnloaded(Scene scene)
    {
        GlobaGameplaylEvents.ClearAllEventsSuscriptions();
    }

    public void SetMode(GameMode mode)
    {
        currentMode = mode;
    }

    public GameMode CurrentMode { get => currentMode; }
    public DeviceType CurrentActiveDevice { get => currentActiveDevice; }
    public InputMode CurrentInputMode { get => currentInputMode;}

    public static bool IsInTutorial()
    {
        if (Instance == null)
        {
            return false;
        }
        return Instance.CurrentMode == GameMode.Tutorial;
    }
}
public enum GameMode
{
    Normal = 0,
    Tutorial = 1
}

public enum InputMode
{
    UI = 0,
    Gameplay = 1
}

public enum DeviceType
{
    none = 0,
    keyboardMouse = 1,
    XboxGamepad = 2,
    playGamepad = 3
}