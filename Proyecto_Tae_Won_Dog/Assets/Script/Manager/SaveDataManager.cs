﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class SaveDataManager : Singleton<SaveDataManager>
{
    private const string HIGH_SCORE_KEY = "HIGH_SCORE_KEY";
    private const string RANK_SCORE_LEADERBOARD_KEY = "RANK_SCORE_KEY";
    private const string CONFIG_IS_ON_SOUND = "CONFIG_ON_SOUND";
    private const string CONFIG_IS_ON_MUSIC = "CONFIG_ON_MUSIC";
    private const string CONFIG_PLAY_TUTO = "CONFIG_PLAY_TUTO";
    private const string CONFIG_LENGUAGE = "CONFIG_LENGUAGE";

    private bool isOnMusic;
    private bool isOnSound;
    private bool wasPlayedTuto;


    private void Start()
    {
        LoadSetting();
    }

    private void LoadSetting()
    {
        isOnMusic = PlayerPrefs.GetInt(CONFIG_IS_ON_MUSIC, 1) == 1;
        isOnSound = PlayerPrefs.GetInt(CONFIG_IS_ON_SOUND, 1) == 1;
        wasPlayedTuto = PlayerPrefs.GetInt(CONFIG_PLAY_TUTO, 0) == 1;
        var lenguage = (LocaleTypes)PlayerPrefs.GetInt(CONFIG_LENGUAGE, 0);
        LocalizationManager.Instance.ChangeLocale(lenguage);
    }

#if UNITY_EDITOR
    private void Update()
    {
      //if (Input.GetKeyUp(KeyCode.R)) //reset save.
      //  {
      //      SaveRankScore(0);
      //      SaveHighScore(0);
      //      SavePlayTutoSetting(false);
      //  }
    }
#endif

    public void SaveLenguageSetting(LocaleTypes type)
    {
        PlayerPrefs.SetInt(CONFIG_LENGUAGE, (int)type);
    }

    public void SaveSoundSetting(bool isOn)
    {
        PlayerPrefs.SetInt(CONFIG_IS_ON_SOUND, isOn ? 1 : 0);
        isOnSound = isOn;
    }
    public void SaveMusicSetting(bool isOn)
    {
        PlayerPrefs.SetInt(CONFIG_IS_ON_MUSIC, isOn ? 1 : 0);
        isOnMusic = isOn;
    }
    public void SavePlayTutoSetting(bool wasPlayed)
    {
        PlayerPrefs.SetInt(CONFIG_PLAY_TUTO, wasPlayed ? 1 : 0);
        wasPlayedTuto = wasPlayed;
    }

    public void SaveRankScore(int value)
    {
        PlayerPrefs.SetInt(RANK_SCORE_LEADERBOARD_KEY, value);
    }
    public int GetRankScore()
    {
        return PlayerPrefs.GetInt(RANK_SCORE_LEADERBOARD_KEY, 0);
    }

    public void SaveHighScore(int value)
    {
        PlayerPrefs.SetInt(HIGH_SCORE_KEY, value);
    }
    public int GetHighScore()
    {
        return PlayerPrefs.GetInt(HIGH_SCORE_KEY, 0);
    }

    public bool IsOnMusic { get => isOnMusic; }
    public bool IsOnSound { get => isOnSound; }
    public bool WasPlayTuto { get => wasPlayedTuto; }
}
