﻿using System;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class SocialManager : Singleton<SocialManager>
{
    TKDSocialBase activeSocialService;

    protected override void Awake()
    {
        base.Awake();
        activeSocialService = GetSocialService();
        activeSocialService.Init();
        LoadPlayerScore((rank) => { });
    }

    private TKDSocialBase GetSocialService()
    {
        TKDSocialBase tKDSocialBase = null;
#if UNITY_ANDROID
        tKDSocialBase = new TKDGoogleSocial();
#elif UNITY_STANDALONE_WIN
        var leaderboardHandler = GameObject.FindObjectOfType<LeaderboardHandler>();
        var trophyHandler = GameObject.FindObjectOfType<TrophyPopupHandler>();

        tKDSocialBase = new TKDSteamSocial(leaderboardHandler, trophyHandler);
#else
        tKDSocialBase = new TKDDummySocial();
#endif
        return tKDSocialBase;
    }

    public void LoadPlayerScore(Action<int> scoreRankCallback)
    {
        activeSocialService.LoadPlayerScore((score)=>
        {
            if (score == null) 
            {
                scoreRankCallback(0);
                return;
            }

            SaveDataManager.Instance.SaveRankScore(score.rank);
            SaveDataManager.Instance.SaveHighScore((int)score.value);
            scoreRankCallback(score.rank);
        });
    }

    public void ShowLeaderboard()
    {
        activeSocialService.ShowLeaderboard();
    }
    public void ShowArchievement()
    {
        activeSocialService.ShowArchievement();
    }

    public void CheckPlankDestroyArchivment(int plankDestroy)
    {
        if (plankDestroy <= 0 || SettingConfigManager.IsInTutorial())
        {
            return;
        }
        activeSocialService.TryReportIncrementalArchivement(activeSocialService.GeTrophyPlanksId(), plankDestroy);
    }

    public void CheckAllScoresArchivement(int score, System.Action OnEndCheck)
    {
        if (score <= 0 || SettingConfigManager.IsInTutorial())
        {
            OnEndCheck();
            return;
        }
        activeSocialService.CheckArchivementProgress(score, OnEndCheck);
    }

    public void CheckHitArchivement(int score, System.Action OnEndCheck)
    {
        if (score <= 0 || SettingConfigManager.IsInTutorial())
        {
            OnEndCheck();
            return;
        }
        activeSocialService.TryReportArchivementScore(activeSocialService.GeTrophyHitId(), score, OnEndCheck);
    }

    public void UpdateScoreLeaderBoard(int newScore, System.Action onComplete)
    {
        if (newScore <= 0)
        {
            onComplete();
            return;
        }
        activeSocialService.UpdateScoreLeaderBoard(newScore, onComplete);
    }

}
