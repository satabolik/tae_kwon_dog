﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class GameplayManager : Singleton<GameplayManager>
{
    [SerializeField]
    private SC_ShakeConfig hitPlayerShakeConfig;
    [SerializeField]
    private SC_ShakeConfig hitPlankShakeConfig;
    [SerializeField]
    private float gameoverDelay = 0.15f;
    [SerializeField]
    private int playerLifePoint;
    [SerializeField]
    private ChangeColorPlayer colorChanger;
    [SerializeField]
    private ScaleTimeController scaleTimeCtrll;
    [SerializeField]
    private float speedUpChange = 0.1f;
    [SerializeField]
    private double speedUpChangeIncrement = 0.1f;
    [SerializeField]
    private float speedUpMaxValue = 0.75f;
    [SerializeField]
    private float speedDownPlayerGetHit = 0.5f;
    [SerializeField]
    private float speedDownPlayerSpecialHit = 0.6f;
    [SerializeField]
    private int changeColorPoint = 15;
    [SerializeField]
    private float changeColorIncrement = 2;
    [SerializeField]
    private int specialModeCountReq = 20;
    [SerializeField]
    private float specialModeTime = 5f;
    private int currentPoint;
    private bool isSpecialModeActive = false;
    private bool isSpecialModeFill = false;
    private int currentSpecialCount = 0;
    private int nextReqLevel = 0;
    private bool isGameOver;
    private int placksDestroyCount = 0;
    private const byte MAX_LIFE_ADD = 8;
    private int hitCounter = 0;
    private int maxHitCounterReached = 0;
    private bool enableJump;
    private bool enableAttack;
    private bool enableRuning;
    private bool enableDamage;
    private int currentLevel = 0;

    protected override void Awake()
    {
        EnableAllPlayerMove(true);
        nextReqLevel = changeColorPoint;
    }


    public void EnableAllPlayerMove(bool enable)
    {
        enableAttack = enable;
        enableJump = enable;
        enableRuning = enable;
        enableDamage = enable;
    }


    private void Start()
    {
        GlobaGameplaylEvents.OnUpdateSpecial(currentSpecialCount, specialModeCountReq);
    }

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnPlayerGetHit += OnPlayerGetHit;
        GlobaGameplaylEvents.OnPlayerFallOnFloor += ResetComboCount;
    }

    private void OnDisable()
    {
        GlobaGameplaylEvents.OnPlayerGetHit -= OnPlayerGetHit;
        GlobaGameplaylEvents.OnPlayerFallOnFloor -= ResetComboCount;

    }


    private void ResetComboCount()
    {
        if (hitCounter > 0)
        {
            if (hitCounter > maxHitCounterReached)
            {
                maxHitCounterReached = hitCounter;
            }
            
            hitCounter = 0;
            GlobaGameplaylEvents.OnUpdateHitCount(0);
        }
    }

    public void AddHitComboCounter()
    {
        GlobaGameplaylEvents.OnUpdateHitCount(++hitCounter);
    }

    private void OnPlayerGetHit(int damage)
    {
        if (isGameOver)
            return;
        if (playerLifePoint == 0)
            return;
        if (!enableDamage)
            return;
        RestarEspecial();
        ShakeUtil.CameraShakeOnce(hitPlayerShakeConfig);
        scaleTimeCtrll.AddTempSlowMotion(speedDownPlayerGetHit);
        playerLifePoint -= damage;
        GlobaGameplaylEvents.OnUpdateLifePlayer(false);
        if (playerLifePoint <= 0)
        {
            if (SettingConfigManager.IsInTutorial())
            {
                AddLifePlayer();
                return;
            }

            playerLifePoint = 0;
            Invoke("GameOver", gameoverDelay);///<see cref="GameOver"/>
        }
    }

    public void AddLifePlayer()
    {
        if (IsInMaxLife())
            return;
        playerLifePoint++;
        SoundManager.Instance.PlaySfx(SfxType.lifeItemPick);
        GlobaGameplaylEvents.OnUpdateLifePlayer(true);
    }

    public bool IsInMaxLife()
    {
        return playerLifePoint >= MAX_LIFE_ADD;
    }

    private void GameOver()
    {
        isGameOver = true;
        GlobaGameplaylEvents.OnGameOver();
    }

#if UNITY_EDITOR
    private void Update()
    {
        if (Keyboard.current.upArrowKey.IsPressed()) ///debug speed up. next score
        {
            int It = Keyboard.current.numpad0Key.IsPressed() ? 100 : 1;

            for (int i = 0; i < It; i++)
            {
                int reqScoreAdd = nextReqLevel - currentPoint;
                Debug.Log($"Add score debug: {reqScoreAdd}, current speed x{speedUpChange}");
                AddScore(reqScoreAdd);
            }

        }
        if (Keyboard.current.rightArrowKey.IsPressed()) ///debug life up
        {
            Debug.Log($"Add life debug");
            AddLifePlayer();
        }

        if (Keyboard.current.leftArrowKey.IsPressed()) ///debug life up
        {
            Debug.Log($"fill special move");
            while (CurrentSpecialCount < SpecialModeCountReq)
            {
                AddCountSpecial();
            }
        }

        
    }
#endif

    public void AddScore(int point, bool applyHitMultiplier = true)
    {
        if (SettingConfigManager.IsInTutorial())
        {
            return;
        }

        currentPoint += applyHitMultiplier && hitCounter > 0?  point * hitCounter : point;
        GlobaGameplaylEvents.OnUpdateScore(point, currentPoint);
        while (currentPoint >= nextReqLevel)
        {
            currentLevel++;
            var dinamicInc = (int)(nextReqLevel * changeColorIncrement);
            nextReqLevel += (changeColorPoint + dinamicInc);
            colorChanger.AddNextColorChange();
            if (Time.timeScale < speedUpMaxValue)
            {
                scaleTimeCtrll.AddSpeed(speedUpChange);
                speedUpChange += (float) speedUpChangeIncrement;
            }

            Debug.Log($"Updated score. Level: {currentLevel}, Score: {currentPoint}, TimeScale: {Time.timeScale}, Next Score: {nextReqLevel}");
        }
    }

    public void ProcessAttackPlank(int boardPoint)
    {
        AddCountPlankDestroy();
        AddHitComboCounter();
        AddScore(boardPoint);
        GlobaGameplaylEvents.OnAttackPlank();
    }


    public void AddCountPlankDestroy()
    {
        ShakeUtil.CameraShakeOnce(hitPlankShakeConfig);
        placksDestroyCount++;
        AddCountSpecial();
        if (isSpecialModeActive)
        {
            scaleTimeCtrll.AddTempSlowMotion(speedDownPlayerSpecialHit);
        }
    }

    public void AddCountSpecial()
    {
        if (!isSpecialModeFill)
        {
            currentSpecialCount++;
            GlobaGameplaylEvents.OnUpdateSpecial(currentSpecialCount, specialModeCountReq);
            if (currentSpecialCount == specialModeCountReq)
            {
                isSpecialModeFill = true;
            }
        }
    }

    public bool RestarEspecial()
    {
        if (currentSpecialCount > 0 && !isSpecialModeFill)
        {
            currentSpecialCount--;
            GlobaGameplaylEvents.OnUpdateSpecial(currentSpecialCount, specialModeCountReq);
            return true;
        }
        return false;
    }

    public void StartSpecialMode()
    {
        if (!isSpecialModeFill)
            return;
        isSpecialModeActive = true;
        GlobaGameplaylEvents.OnActiveSpecial();
        StartCoroutine(RestablecerEspecial());
    }

    private IEnumerator RestablecerEspecial()
    {
        yield return new WaitForSeconds(specialModeTime);
        currentSpecialCount = 0;
        GlobaGameplaylEvents.OnUpdateSpecial(currentSpecialCount, specialModeCountReq);
        isSpecialModeActive = false;
        isSpecialModeFill = false;
        GlobaGameplaylEvents.OnSpecialOver();

    }



    public int CurrentPoint { get => currentPoint; }
    public bool IsGameOver { get => isGameOver; }
    public int PlayerLifePoint { get => playerLifePoint; }
    public float SpecialModeTime { get => specialModeTime; }
    public bool IsSpecialModeActive { get => isSpecialModeActive; }
    public int PlacksDestroyCount { get => placksDestroyCount; }
    public int MaxHitCounterReached { get => maxHitCounterReached; }
    public ScaleTimeController ScaleTimeCtrll { get => scaleTimeCtrll; }
    public int HitCounter { get => hitCounter; }
    public bool EnableJump { get => enableJump; set => enableJump = value; }
    public bool EnableAttack { get => enableAttack; set => enableAttack = value; }
    public bool EnableRuning { get => enableRuning; set => enableRuning = value; }
    public bool EnableDamage { get => enableDamage; set => enableDamage = value; }
    public bool IsSpecialModeFill { get => isSpecialModeFill; }
    public int CurrentSpecialCount { get => currentSpecialCount; }
    public int SpecialModeCountReq { get => specialModeCountReq; }
}