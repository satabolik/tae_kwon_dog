﻿using System;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-50)]


public class LocalizationManager : Singleton<LocalizationManager>
{
    [SerializeField]
    private SC_LocalizationConfig[] localizationsConfigs;
    private LocaleTypes currentLocale;
    private SC_LocalizationConfig currentConfigUsed;


    public event Action OnLocaleUpdate = delegate{ };

    public string TryGetNearLocaleKey(string requestKey)
    {
        if (localizationsConfigs.Length == 0 || string.IsNullOrEmpty(requestKey))
            return null;
        
        string mostNearKey = null;
        float mostNearValue = 0.5f;

        foreach (var localizationConfig in localizationsConfigs)
        {
            foreach (var locValue in localizationConfig.LocaleValues)
            {
                if (locValue.key.Contains(requestKey, StringComparison.OrdinalIgnoreCase))
                {
                    return locValue.key;
                }

                float MatchedPercent = GetMatchPercentage(locValue.key, requestKey);
                if (MatchedPercent > mostNearValue)
                {
                    mostNearKey = locValue.key;
                    mostNearValue = MatchedPercent;
                }
            }
        }
        
        return mostNearKey;
    }

    private void LoadDefaulLenguage()
    {
        var defaultLocale = LocaleTypes.enUS;
        if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            defaultLocale = LocaleTypes.esCL;
        }
       
        ChangeLocale(defaultLocale);
    }


    public void ChangeLocale(LocaleTypes type)
    {
        currentLocale = type;
        if (type == LocaleTypes.NONE)
        {
            LoadDefaulLenguage();
            return;
        }
        Debug.Log($"Load lenguage: <b>{type}</b>");
        currentConfigUsed = GetLocaleConfig(type);
        OnLocaleUpdate();
    }

    public string GetText(string keyLocale)
    {
        if (currentConfigUsed == null || string.IsNullOrWhiteSpace(keyLocale))
            return null;
        return currentConfigUsed.GetLocValue(keyLocale);
    }

    private SC_LocalizationConfig GetLocaleConfig(LocaleTypes type)
    {
        if (localizationsConfigs == null)
            return null;
        foreach (var config in localizationsConfigs)
        {
            if (config.LocaleType == type)
                return config;
        }
        return null;
    }

    /// <summary>
    /// Chat gpt function
    /// </summary>
    /// <param name="str1"></param>
    /// <param name="str2"></param>
    /// <returns></returns>
    public static float GetMatchPercentage(string str1, string str2)
    {
        // Si ambos strings son vacíos, el match es 1
        if (string.IsNullOrEmpty(str1) && string.IsNullOrEmpty(str2))
            return 1f;

        // Si uno de los strings es vacío, el match es 0
        if (string.IsNullOrEmpty(str1) || string.IsNullOrEmpty(str2))
            return 0f;

        int minLength = Mathf.Min(str1.Length, str2.Length); // Longitud del string más corto
        int maxLength = Mathf.Max(str1.Length, str2.Length); // Longitud del string más largo
        int matchCount = 0;

        // Contar cuántos caracteres coinciden en la misma posición
        for (int i = 0; i < minLength; i++)
        {
            if (str1[i] == str2[i])
            {
                matchCount++;
            }
        }

        // Retornar el porcentaje de coincidencia como un valor entre 0 y 1
        return (float)matchCount / maxLength;
    }

    public LocaleTypes CurrentLocale { get => currentLocale; }

}
[System.Serializable]
public struct LocalePair
{
    public string key;
    [TextArea]
    public string value;
}
public enum LocaleTypes
{
    NONE = 0,
    esCL = 1,
    enUS = 2,
}
