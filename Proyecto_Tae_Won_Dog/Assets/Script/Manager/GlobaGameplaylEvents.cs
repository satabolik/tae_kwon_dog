﻿using System;

public static class GlobaGameplaylEvents
{
    public static Action OnGameOver = delegate { };
    public static Action OnGameOverShowCompleted = delegate { };

    public static Action OnActiveSpecial = delegate { };
    public static Action OnSpecialOver = delegate { };
    public static Action<int, int> OnUpdateScore = delegate { };
    public static Action<int> OnUpdateHitCount = delegate { };
    public static Action<int> OnPlayerGetHit = delegate { };
    public static Action<int, int> OnUpdateSpecial = delegate { };
    public static Action<float> OnPlayerJump = delegate { };
    public static Action OnPlayerAttack = delegate { };
    public static Action OnAttackPlank = delegate { };
    public static Action OnPlayerFallOnFloor = delegate { };
    /// <summary>
    /// llamado al actualizar vida de jugador (true si se agrega, false si se quita)
    /// </summary>
    public static Action<bool> OnUpdateLifePlayer = delegate { };
    public static Action OnStartRun = delegate { };
    public static Action OnDestroyBoard = delegate { };



    public static void ClearAllEventsSuscriptions()
    {
        OnGameOver = delegate { };
        OnActiveSpecial = delegate { };
        OnUpdateScore = delegate { };
        OnUpdateHitCount = delegate { };
        OnPlayerGetHit = delegate { };
        OnUpdateSpecial = delegate { };
        OnPlayerJump = delegate { };
        OnPlayerAttack = delegate { };
        OnAttackPlank = delegate { };
        OnPlayerFallOnFloor = delegate { };
        OnUpdateLifePlayer = delegate { };
        OnStartRun = delegate { };
        OnDestroyBoard = delegate { };
        OnGameOverShowCompleted = delegate { };
    }
}
