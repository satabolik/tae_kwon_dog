﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class MainMenuCtrll : MonoBehaviour
{
    [SerializeField]
    private PopupHandler popupTutorial;
    [SerializeField]
    private PopupHandler popupCloseGame;
    [SerializeField]
    private SettingHandler setting;
    [SerializeField]
    private Button btnSetting;
    [SerializeField]
    private Button btnTutorial;
    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Button btnLeaderboard;
    [SerializeField]
    private Button btnAchievement;
    [SerializeField]
    private Button btnCloseGame;

    // Start is called before the first frame update
    void Start()
    {
        btnPlay.onClick.AddListener(OnClickPlay);
        btnLeaderboard.onClick.AddListener(OnClickLeaderboard);
        btnAchievement.onClick.AddListener(OnClickArchivement);
        btnSetting.onClick.AddListener(OnClickSetting);
        btnTutorial.onClick.AddListener(OnClickTutorialButton);
        btnCloseGame.onClick.AddListener(OnClickCloseGame);

        SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        ButtonLayoutManager.Instance?.ActiveButtonLayoutPanel(LayoutKeyEnum.MainMenu);
    }

    private void OnEnable()
    {
        BindingShortcutSetting();
        setting.OnCancelEvent += BindingShortcutSetting;
    }

    private void OnDisable()
    {
        UnbindingShortcutSetting();
        setting.OnCancelEvent -= BindingShortcutSetting;
    }

    private void BindingShortcutSetting()
    {
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnUIOpenSetting += OnClickSetting;
    }

    private void UnbindingShortcutSetting()
    {
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnUIOpenSetting -= OnClickSetting;
    }

    private void OnClickSetting()
    {
        if (setting.ShowPopUp(true))
        {
            UnbindingShortcutSetting();
            SoundManager.Instance.PlaySfx(SfxType.PressButton);
        }
    }

    private void OnClickCloseGame()
    {
        popupCloseGame.ClearEvent();

        Action OnPostClickPlay = () =>
        {
            popupCloseGame.OnAcceptEvent += CloseGame;
            popupCloseGame.OnCancelEvent += ()=> popupCloseGame.ShowPopUp(false);
            popupCloseGame.ShowPopUp(true);
        };

        SoundManager.Instance.PlaySfx(SfxType.PressButton, OnPostClickPlay);
    }

    private void OnClickPlay()
    {
        btnPlay.interactable = false;
        Action OnPostClickPlay = () =>
        {
            var wasPlayTuto = SaveDataManager.Instance.WasPlayTuto;
            if (!wasPlayTuto)
            {
                popupTutorial.OnAcceptEvent += LoadTutorial;
                popupTutorial.OnCancelEvent += LoadNormaPlayl;
                popupTutorial.ShowPopUp(true);
            }
            else
            {
                LoadNormaPlayl();
            }
        };

        SoundManager.Instance.PlaySfx(SfxType.PressButton, OnPostClickPlay);
    }

    private void OnClickTutorialButton()
    {
        SoundManager.Instance.PlaySfx(SfxType.PressButton);
        popupTutorial.OnAcceptEvent += LoadTutorial;
        popupTutorial.OnCancelEvent += ()=>
        {
            popupTutorial.ShowPopUp(false);

        };
        popupTutorial.ShowPopUp(true);
    }

    private void OnClickLeaderboard()
    {
        SoundManager.Instance.PlaySfx(SfxType.PressButton, SocialManager.Instance.ShowLeaderboard);
    }
    private void OnClickArchivement()
    {
        SoundManager.Instance.PlaySfx(SfxType.PressButton, SocialManager.Instance.ShowArchievement);
    }

    private void LoadGameplay()
    {
        SceneManager.LoadScene(ScenesNames.GAMEPLAY);
    }

    private void CloseGame()
    {
        Debug.Log("CloseGame!");
        Application.Quit();
    }

    private void LoadNormaPlayl()
    {
        SettingConfigManager.Instance.SetMode(GameMode.Normal);
        LoadGameplay();
    }

    private void LoadTutorial()
    {
        SettingConfigManager.Instance.SetMode(GameMode.Tutorial);
        LoadGameplay();
    }
}
