﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;

public class TutorialHandler : MonoBehaviour
{
    [SerializeField]
    private float delayShowDialog = 1f;
    [SerializeField]
    private PopupHandler popupEndTutorial;
    [SerializeField]
    private AbstractStepTutorial[] stepsTutorial;
    [SerializeField]
    private GameplayMainHudHandler mainHud;
    [SerializeField]
    private NpcItemSpawner npcSpawner;

    private int currentStepIndex;
    private AbstractStepTutorial currentStep;
    private WaitForSecondsRealtime waitRealSecondShowPopup; 
    // Start is called before the first frame update
    void Start()
    {
        if (stepsTutorial != null)
            foreach (var step in stepsTutorial)
                step.gameObject.SetActive(false);
        if (SettingConfigManager.IsInTutorial())
            InitTutorialSetting();
    }

    private void NextStepTutorial()
    {
        StartCoroutine(NextStepTutorialRutine());
    }

    private IEnumerator NextStepTutorialRutine()
    {
        yield return waitRealSecondShowPopup;
        if (currentStepIndex >= stepsTutorial.Length)
        {
            OnEndTutorial();
            yield break;
        }
        if (currentStep != null)
        {
            currentStep.gameObject.SetActive(false);
        }
        currentStep = stepsTutorial[currentStepIndex++];
        currentStep.gameObject.SetActive(true);
        Debug.Log($"Start TUTORAL: {currentStep.gameObject.name}");
        currentStep.StartStep(NextStepTutorial);
    }

    private void OnEndTutorial()
    {
        popupEndTutorial.OnAcceptEvent += OnClickPlayMode;
        popupEndTutorial.OnCancelEvent += OnClickRetryTutorial;
        popupEndTutorial.ShowPopUp(true);
        SaveDataManager.Instance.SavePlayTutoSetting(true);
        if (SettingConfigManager.Instance != null)
        {
            SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        }
    }

#if UNITY_EDITOR
    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    npcSpawner.SpawnNpc(TypeLevel.Bot);
        //}
        //if (Input.GetKeyDown(KeyCode.Alpha2))
        //{
        //    npcSpawner.SpawnNpc(TypeLevel.Mid);
        //}
        //if (Input.GetKeyDown(KeyCode.Alpha3))
        //{
        //    npcSpawner.SpawnNpc(TypeLevel.Top);
        //}
        if (Keyboard.current.nKey.wasReleasedThisFrame)
        {
            Debug.Log("Debug next step tutorial");
            NextStepTutorial();
        }
    }

#endif

    private void InitTutorialSetting()
    {
        npcSpawner.ManualSpawn = true;
        mainHud.ViewComboHit(false);
        mainHud.ViewLife(false);
        mainHud.ViewScore(false);
        mainHud.ViewPowerUp(false);
        GameplayManager.Instance.EnableAllPlayerMove(false);
        waitRealSecondShowPopup = new WaitForSecondsRealtime(delayShowDialog);
        NextStepTutorial();
    }


    private void OnClickPlayMode()
    {
        SettingConfigManager.Instance.SetMode(GameMode.Normal);
        LoadScene();
    }

    private void OnClickRetryTutorial()
    {
        SettingConfigManager.Instance.SetMode(GameMode.Tutorial);
        LoadScene();

    }

    private void LoadScene()
    {
        SoundManager.Instance.PlaySfx(SfxType.PressButton);
        SceneManager.LoadScene(ScenesNames.GAMEPLAY);
    }
}
