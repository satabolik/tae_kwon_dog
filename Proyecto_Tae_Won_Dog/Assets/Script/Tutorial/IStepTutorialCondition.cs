﻿using System;

public interface IStepTutorialCondition
{
    void StartStep(Action onComplete);
}
