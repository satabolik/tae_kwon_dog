﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeStepTutorial : AbstractStepTutorial
{
    [SerializeField]
    private Image imgLifeHL;
    [SerializeField]
    private ScaleTimeController scaleTimer;
    [SerializeField]
    private GameplayMainHudHandler mainHud;
    [SerializeField]
    private NpcItemSpawner npcSpawner;
    private IEnumerator PinPonHighlighRutine;

    public override void StartStep(Action onComplete)
    {
        StartTempTouchBlock(timeBlockTouchBeforeDialog);
        StartCoroutine(RuningStepRutine(onComplete));
    }

    protected override IEnumerator RuningStepRutine(Action onComplete)
    {
        GameplayManager.Instance.EnableJump = false;
        GameplayManager.Instance.EnableDamage = true;
        mainHud.ViewLife(true);
        npcSpawner.SpawnNpc(TypeLevel.Bot);
        yield return CorutineCallbackEventTrigger.WaitPlayerGetHitEvent();

      
        scaleTimer.AddTempSlowMotion(0.99f, false, 0.1f);
        //mensaje de golpe     
        tutoDialog.ShowDialog(GetNextText());
        imgLifeHL.enabled = true;
        PinPonHighlighRutine = CorutinePingPong.StartPingPong(2.5f, 1f, (v) => GlobalSetAlpha.Set(imgLifeHL, v));
        yield return PinPonHighlighRutine;
        imgLifeHL.enabled = false;
        tutoDialog.ShowTouchToContinue();
        yield return WaitTouchBlock();
       
        //mensaje explicando el hud de vida
        tutoDialog.Hide();
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);

        tutoDialog.ShowDialog(GetNextText(), true);
        yield return WaitTouchBlock();
       
        //consecuencia de perder todas las vidas    
        tutoDialog.Hide();
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);

        tutoDialog.ShowDialog(GetNextText(), true);
        yield return WaitTouchBlock();
        tutoDialog.Hide();
        scaleTimer.ResumeCurrentTimeScale();
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        onComplete();
    }

    protected override void OnDisable()
    {
        if (PinPonHighlighRutine != null)
        {
            StopCoroutine(PinPonHighlighRutine);
        }
        scaleTimer.ResumeCurrentTimeScale();
        base.OnDisable();
    }
}
