﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameStepTutorial : AbstractStepTutorial
{

    public override void StartStep(Action onComplete)
    {
        StartTempTouchBlock(timeBlockTouchBeforeDialog);
        StartCoroutine(RuningStepRutine(onComplete));
    }

    protected override IEnumerator RuningStepRutine(Action onComplete)
    {      
        handIconHandler.PlayTouchAnim();
        tutoDialog.ShowDialog(GetNextText());
        GameplayManager.Instance.EnableRuning = true;
        yield return CorutineCallbackEventTrigger.WaitPlayerRun();
        tutoDialog.Hide();
        handIconHandler.ResetAnim();
        onComplete();
    }
}
