﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JumpStepTutorial : AbstractStepTutorial
{
    [SerializeField]
    private int jumpCountRequired;
    [SerializeField]
    private MinMaxValue[] orderStepsJumpValue;
    [SerializeField]
    private Image imgLowBar;
    [SerializeField]
    private Image imgMidBar;
    [SerializeField]
    private Image imgHighBar;
    [SerializeField]
    private Color colorBaseBar;
    [SerializeField]
    private Color colorCheckBar;
    private int currentJumpCount;
    private Image currentBarImage;
    private MinMaxValue? currentJumpMinMaxValue;

    private void Awake()
    {
        imgLowBar.enabled = false;
        imgMidBar.enabled = false;
        imgHighBar.enabled = false;
        imgLowBar.color = colorBaseBar;
        imgMidBar.color = colorBaseBar;
        imgHighBar.color = colorBaseBar;
    }

    public override void StartStep(Action onComplete)
    {
        StartTempTouchBlock(timeBlockTouchBeforeDialog);
        StartCoroutine(RuningStepRutine(onComplete));
    }

    protected override IEnumerator RuningStepRutine(Action onComplete)
    {
        
        tutoDialog.ShowDialog(GetNextText());
        handIconHandler.PlayHoldAnim();
        GameplayManager.Instance.EnableJump = true; ;
        yield return CorutineCallbackEventTrigger.WaitJumpEvent();
        yield return CorutineCallbackEventTrigger.WaitPlayerFallFloorEvent();
        handIconHandler.ResetAnim();
        tutoDialog.ShowDialog(GetNextText(), true);
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        yield return WaitTouchBlock();
        tutoDialog.Hide();
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);


        GlobaGameplaylEvents.OnPlayerJump += OnPlayerJumpStep;
        void handleHudCount()
        {
            tutoDialog.UpdateStepsCount(currentJumpCount, jumpCountRequired);
            if (currentBarImage != null)
                currentBarImage.color = colorBaseBar;
        };
        GlobaGameplaylEvents.OnPlayerFallOnFloor += handleHudCount;
        var imgsBars = new Image[] { imgLowBar, imgMidBar, imgHighBar };

        for (int i = 0; i < orderStepsJumpValue.Length; i++)
        {
            currentJumpMinMaxValue = orderStepsJumpValue[i];
            currentBarImage = imgsBars[i];
            yield return JumpStepTestRutine();
        }
        GlobaGameplaylEvents.OnPlayerFallOnFloor -= handleHudCount;
        GlobaGameplaylEvents.OnPlayerJump -= OnPlayerJumpStep;
        tutoDialog.Hide();
        onComplete();
    }

    private IEnumerator JumpStepTestRutine()
    {
        currentJumpCount = 0;
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText());
        currentBarImage.enabled = true;
        tutoDialog.UpdateStepsCount(currentJumpCount, jumpCountRequired);
        while (currentJumpCount < jumpCountRequired)
            yield return null;
        yield return CorutineCallbackEventTrigger.WaitPlayerFallFloorEvent();
        currentBarImage.enabled = false;
    }

    private void OnPlayerJumpStep(float jumpForce)
    {
        if (currentJumpMinMaxValue != null)
        {
            var valReq = currentJumpMinMaxValue.Value;
            if (jumpForce >= valReq.minValue && jumpForce <= valReq.maxValue)
            {
                currentBarImage.color = colorCheckBar;
                currentJumpCount++;
            }
        }
    }

    protected override void OnDisable()
    {
        if (imgLowBar != null) imgLowBar.enabled = false;
        if (imgMidBar != null) imgMidBar.enabled = false;
        if (imgHighBar != null) imgHighBar.enabled = false;
        if (currentBarImage != null) currentBarImage.enabled = false;
        base.OnDisable();
    }
}
