﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpStepTutorial : AbstractStepTutorial
{
    [SerializeField]
    private GameplayMainHudHandler mainMenu;
    [SerializeField]
    private float timeChargePowerUp = 0.25f;
    [SerializeField]
    private float timeSpawnNpc = 1f;
    [SerializeField]
    private NpcItemSpawner spawnnerNps;
    private const int COMBO_REQUIRED = 3;
    private int countCombo;
    private WaitForSecondsRealtime secondWaitSpawnNpc;

    private void Start()
    {
        secondWaitSpawnNpc = new WaitForSecondsRealtime(timeSpawnNpc);
    }
    public override void StartStep(Action onComplete)
    {
        StartCoroutine(RuningStepRutine(onComplete));
    }

    protected override IEnumerator RuningStepRutine(Action onComplete)
    {
        var gm = GameplayManager.Instance;
        gm.EnableAllPlayerMove(true);

        tutoDialog.ShowDialog(GetNextText(), true);
        mainMenu.ViewPowerUp(true);
        mainMenu.ViewLife(true);
        yield return WaitTouchBlock();

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText());
        while (gm.CurrentSpecialCount < gm.SpecialModeCountReq - 1)
        {
            gm.AddCountSpecial();
            yield return TempTouchBlock(timeChargePowerUp);
        }
        tutoDialog.ShowTouchToContinue();
        yield return WaitTouchBlock();


        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText(), true);
        gm.AddCountSpecial();
        yield return WaitTouchBlock();

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText(), true);
        yield return WaitTouchBlock();

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText());
        yield return CorutineCallbackEventTrigger.WaitSpecialActive();

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText());

        yield return ComboSpecialAttackRutine();

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.ShowDialog(GetNextText(), true);
        yield return WaitTouchBlock();


        tutoDialog.Hide();
        onComplete();
    }

    private IEnumerator ComboSpecialAttackRutine()
    {
        tutoDialog.UpdateStepsCount(0, COMBO_REQUIRED);
        countCombo = 0;
        var comboFinish = false;
        var skipWaitTouchFloor = false;
        void handleAttackPlank()
        {
            if (comboFinish)
                return;
            countCombo++;
            if (countCombo >= COMBO_REQUIRED)
                comboFinish = true;
            tutoDialog.UpdateStepsCount(countCombo, COMBO_REQUIRED);
        }
        void HandleTouchFloor()
        {
            if (comboFinish)
            {
                skipWaitTouchFloor = true;
                return;
            }
            countCombo = 0;
            tutoDialog.UpdateStepsCount(countCombo, COMBO_REQUIRED);
        }
        void HandlePowerUpEmpty()
        {
            StartCoroutine(RechargePowerUp());
        }

        GlobaGameplaylEvents.OnAttackPlank += handleAttackPlank;
        GlobaGameplaylEvents.OnPlayerFallOnFloor += HandleTouchFloor;
        GlobaGameplaylEvents.OnSpecialOver += HandlePowerUpEmpty;
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        while (!comboFinish)
        {
            spawnnerNps.SpawnNpc(TypeLevel.Mid);
            yield return secondWaitSpawnNpc;
            spawnnerNps.SpawnNpc(TypeLevel.Top);
            yield return secondWaitSpawnNpc;
        }

        GlobaGameplaylEvents.OnAttackPlank -= handleAttackPlank;
        GlobaGameplaylEvents.OnPlayerFallOnFloor -= HandleTouchFloor;
        GlobaGameplaylEvents.OnSpecialOver -= HandlePowerUpEmpty;
        if (!skipWaitTouchFloor)
            yield return CorutineCallbackEventTrigger.WaitPlayerFallFloorEvent();

    }


    private IEnumerator RechargePowerUp()
    {
        var gm = GameplayManager.Instance;
        while (!gm.IsSpecialModeFill)
        {
            gm.AddCountSpecial();
            yield return new WaitForSeconds(timeChargePowerUp);
        }
    }
}
