﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackStepTutorial : AbstractStepTutorial
{
    [SerializeField]
    private Image imgComboHL;
    [SerializeField]
    private GameplayMainHudHandler mainHud;
    [SerializeField]
    private float timeSpawnNpc = 1f;
    [SerializeField]
    private NpcItemSpawner spawnnerNps;
    [SerializeField]
    private ScaleTimeController timeScaler;
    [SerializeField]
    private float slowMotionAttackPercent = 0.1f;
    [SerializeField]
    private float slowMotionAttackTime = 0.1f;
    [SerializeField]
    private float slowMotionImpulseAttackTime = 0.1f;
    [SerializeField]
    private float slowMotionAfterComboPercent = 0.1f;
    private WaitForSecondsRealtime secondWaitSpawnNpc;
    private const int COMBO_REQUIRED = 3;
    private int countCombo;
    private IEnumerator PinPonHighlighRutine;

    private void Start()
    {
        imgComboHL.enabled = false;
        secondWaitSpawnNpc = new WaitForSecondsRealtime(timeSpawnNpc);
    }

    public override void StartStep(Action onComplete)
    {
        StartTempTouchBlock(timeBlockTouchBeforeDialog);
        StartCoroutine(RuningStepRutine(onComplete));
    }

    protected override IEnumerator RuningStepRutine(Action onComplete)
    {
        GameplayManager.Instance.EnableRuning = true;
        GameplayManager.Instance.EnableJump = true;
        yield return StartAttackRutine();
        yield return HitPlankRutine();
        yield return ImpulseAttackrutine();
        yield return ComboAttackRutine();
        onComplete();
    }


    private IEnumerator StartAttackRutine()
    {
        tutoDialog.ShowDialog(GetNextText());
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        yield return CorutineCallbackEventTrigger.WaitJumpEvent();

        tutoDialog.ShowDialog(GetNextText());
        handIconHandler.PlayTouchAnim();
        timeScaler.AddTempSlowMotion(slowMotionAttackPercent, false, slowMotionAttackTime);
        GameplayManager.Instance.EnableAttack = true;
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        yield return CorutineCallbackEventTrigger.WaitAttackEvent();

        timeScaler.ResumeCurrentTimeScale();
        handIconHandler.ResetAnim();
        yield return CorutineCallbackEventTrigger.WaitPlayerFallFloorEvent();

    }

    private IEnumerator HitPlankRutine()
    {
        tutoDialog.ShowDialog(GetNextText());
        var wasAttackToPlank = false;
        void handle() { wasAttackToPlank = true; }
        GlobaGameplaylEvents.OnAttackPlank += handle;
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        while (!wasAttackToPlank)
        {
            spawnnerNps.SpawnNpc(TypeLevel.Bot, true);
            yield return secondWaitSpawnNpc;
        }
        GlobaGameplaylEvents.OnAttackPlank -= handle;

    }

    private IEnumerator ImpulseAttackrutine()
    {
        tutoDialog.ShowDialog(GetNextText());
        yield return null;

        var wasAttackToPlank = false;
        void handle()
        {
            wasAttackToPlank = true;
            GameplayManager.Instance.EnableAttack = false;
            GlobaGameplaylEvents.OnAttackPlank -= handle;
            timeScaler.AddTempSlowMotion(slowMotionAttackPercent, false, slowMotionImpulseAttackTime);
            tutoDialog.ShowDialog(GetNextText());
            handIconHandler.PlayTouchAnim();
        }
        GlobaGameplaylEvents.OnAttackPlank += handle;
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);


        while (!wasAttackToPlank)
        {
            spawnnerNps.SpawnNpc(TypeLevel.Mid);
            yield return secondWaitSpawnNpc;
        }

        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        GameplayManager.Instance.EnableAttack = true;
        yield return CorutineCallbackEventTrigger.WaitAttackEvent();
        timeScaler.ResumeCurrentTimeScale();
        handIconHandler.ResetAnim();
    }

    private IEnumerator ComboAttackRutine()
    {
        tutoDialog.ShowDialog(GetNextText());
        tutoDialog.UpdateStepsCount(0, COMBO_REQUIRED);
        yield return null;
        countCombo = 0;
        mainHud.ViewComboHit(true);
        // mainHud.ViewScore(true);

        var comboFinish = false;
        void handleAttackPlank()
        {
            countCombo++;
            tutoDialog.UpdateStepsCount(countCombo, COMBO_REQUIRED);

            if (countCombo >= COMBO_REQUIRED)
            {
                tutoDialog.ShowDialog(GetNextText());
                timeScaler.AddTempSlowMotion(slowMotionAfterComboPercent, false, slowMotionImpulseAttackTime);
                comboFinish = true;
            }
        }
        GlobaGameplaylEvents.OnAttackPlank += handleAttackPlank;
        void HandleTouchFloor()
        {
            countCombo = 0;
            tutoDialog.UpdateStepsCount(countCombo, COMBO_REQUIRED);
        }
        GlobaGameplaylEvents.OnPlayerFallOnFloor += HandleTouchFloor;
        var waitDelayBtwNpc = new WaitForSeconds(0.6f);
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        while (!comboFinish)
        {
            spawnnerNps.SpawnNpc(TypeLevel.Bot);
            yield return waitDelayBtwNpc;
            spawnnerNps.SpawnNpc(TypeLevel.Mid);
            yield return waitDelayBtwNpc;
            spawnnerNps.SpawnNpc(TypeLevel.Top);
            yield return secondWaitSpawnNpc;
        }
        GlobaGameplaylEvents.OnAttackPlank -= handleAttackPlank;
        GlobaGameplaylEvents.OnPlayerFallOnFloor -= HandleTouchFloor;
        imgComboHL.enabled = true;
        PinPonHighlighRutine = CorutinePingPong.StartPingPong(5f, 1f, (v) => GlobalSetAlpha.Set(imgComboHL, v));
        yield return PinPonHighlighRutine;
        imgComboHL.enabled = false;

        tutoDialog.ShowTouchToContinue();
        yield return WaitTouchBlock();
        timeScaler.ResumeCurrentTimeScale();
        yield return TempTouchBlock(timeBlockTouchBeforeDialog);
        tutoDialog.Hide();

    }

    protected override void OnDisable()
    {
        base.OnDisable();
        timeScaler?.ResumeCurrentTimeScale();
        if (PinPonHighlighRutine != null)
        {
            StopCoroutine(PinPonHighlighRutine);
        }
    }
}
