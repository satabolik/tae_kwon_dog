﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TutorialDialogHandler : MonoBehaviour
{
    [SerializeField]
    private Color hlTextColor;
    [SerializeField]
    private float fadeInDialog = 0.1f;
    [SerializeField]
    private float fadeOutDialog = 0.1f;
    [SerializeField]
    private float fadeTimeTouchToContinue = 2.5f;
    [SerializeField]
    private CanvasGroup cgTouchToContinue;
    [SerializeField]
    private CanvasGroup canvasGpDialog;
    [SerializeField]
    private TMP_Text txtDialog;
    [SerializeField]
    private TMP_Text txtStepsCount;
    [SerializeField]
    private Vector3 scaleAnimStepsTarget;
    [SerializeField]
    private float timeScaleFadeInStep;
    [SerializeField]
    private float timeScaleFadeOutStep;
    [SerializeField]
    private LeanTweenType typeScaleIn;
    [SerializeField]
    private LeanTweenType typeScaleOut;
    [SerializeField]
    protected CustomKV<string, ButtonActionType>[] buttonActionIconByIdTextBox;
    private int tweenFadeText;
    private int tweenStepsText;
    private int tweenFadeTouchToContinue;
    private string currentLocKeyText;

    private const string COLOR_RICHTEXT_PATRON = "color=#{0}";
    private const string HL_TEXT_PATRON = "hl";

    private void Awake()
    {
        cgTouchToContinue.alpha = 0f;
        canvasGpDialog.alpha = 0f;
        txtStepsCount.text = string.Empty;
    }
    private void OnEnable()
    {
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocaleUpdate += UpdateText;
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnDeviceChanged += Instance_OnDeviceChanged;
    }

    private void Instance_OnDeviceChanged(DeviceType obj)
    {
        UpdateText();
    }

    private void OnDisable()
    {
        CancelsTweens();
        if (LocalizationManager.Instance != null)
            LocalizationManager.Instance.OnLocaleUpdate -= UpdateText;
        if (SettingConfigManager.Instance != null)
            SettingConfigManager.Instance.OnDeviceChanged -= Instance_OnDeviceChanged;
    }
    private void CancelsTweens()
    {
        if (LeanTween.isTweening(tweenStepsText))
            LeanTween.cancel(tweenStepsText);
        if (LeanTween.isTweening(tweenFadeTouchToContinue))
            LeanTween.cancel(tweenFadeTouchToContinue);
        if (LeanTween.isTweening(tweenFadeText))
            LeanTween.cancel(tweenFadeText);
    }

    public void UpdateStepsCount(int currentVal, int ReqValue)
    {
        var textToUpdate = $"{currentVal}/{ReqValue}";
        if (txtStepsCount.text == textToUpdate)
            return;
        if (LeanTween.isTweening(tweenStepsText))
            LeanTween.cancel(tweenStepsText);
        txtStepsCount.text = textToUpdate;
        if (currentVal == 0)
        {
            txtStepsCount.gameObject.transform.localScale = Vector3.one;
            return;///no tween con valor 0
        }
        TweenStepIn();
    }

    private void TweenStepIn()
    {
        tweenStepsText = LeanTween.scale(txtStepsCount.gameObject, scaleAnimStepsTarget, timeScaleFadeInStep)
          .setIgnoreTimeScale(true)
          .setEase(typeScaleIn)
          .setOnComplete(TweenStepOut)
          .uniqueId;
    }
    private void TweenStepOut()
    {
        tweenStepsText = LeanTween.scale(txtStepsCount.gameObject, Vector3.one, timeScaleFadeOutStep)
          .setIgnoreTimeScale(true)
          .setEase(typeScaleOut)
          .uniqueId;
    }

    public void ShowDialog(string locKey, bool showTouchToContinue = false)
    {
        if (string.IsNullOrWhiteSpace(locKey))
            return;
        CancelsTweens();
        currentLocKeyText = locKey;
        UpdateText();
        var tween = LeanTween.value(0f, 1f, fadeInDialog)
            .setIgnoreTimeScale(true)
            .setOnUpdate(UpdateAlphaCanvas);
        cgTouchToContinue.alpha = 0f;
        if (showTouchToContinue)
            tween.setOnComplete(ShowTouchToContinue);
        tweenFadeText = tween.uniqueId;
    }
    public void ShowTouchToContinue()
    {
        txtStepsCount.text = string.Empty;
        FadeTouchContinueMessage(true);
    }

    private void UpdateText()
    {
        var textLocale = LocalizationManager.Instance.GetText(currentLocKeyText);
        if (!string.IsNullOrWhiteSpace(textLocale))
        {
            if (textLocale.Contains(HL_TEXT_PATRON))
            {
                var colorHex = ColorUtility.ToHtmlStringRGB(hlTextColor);
                var colorText = string.Format(COLOR_RICHTEXT_PATRON, colorHex);
                textLocale = textLocale.Replace(HL_TEXT_PATRON, colorText);
            }

            TryUpdateIconButtonText(currentLocKeyText, ref textLocale);

        }
        txtDialog.text = textLocale;
    }

    private void TryUpdateIconButtonText(string locKey, ref string inTextLocale)
    {
        if (buttonActionIconByIdTextBox == null || buttonActionIconByIdTextBox.Length == 0)
            return;
        foreach (var actionIdText in buttonActionIconByIdTextBox)
        {
            if (actionIdText.key != locKey)
            {
                continue;
            }

            string spriteNameForText = ButtonLayoutManager.Instance.GetButtonText(actionIdText.value);
            if (string.IsNullOrWhiteSpace(spriteNameForText))
            {
                continue;
            }

            inTextLocale = string.Format(inTextLocale, spriteNameForText);
            return;
        }
    }


    private void FadeTouchContinueMessage(bool fadeIn)
    {

        var startAlpha = (fadeIn) ? 0f : 1f;
        var endAlpha = (!fadeIn) ? 0f : 1f;
        tweenFadeTouchToContinue = LeanTween.value(startAlpha, endAlpha, fadeTimeTouchToContinue)
              .setIgnoreTimeScale(true)
              .setOnUpdate((v) => cgTouchToContinue.alpha = v)
              .setOnComplete(() => FadeTouchContinueMessage(!fadeIn))
              .uniqueId;
    }

    public void Hide()
    {
        CancelsTweens();
        txtStepsCount.text = string.Empty;
        tweenFadeText = LeanTween.value(1f, 0f, fadeOutDialog)
            .setIgnoreTimeScale(true)
            .setOnUpdate(UpdateAlphaCanvas)
            .uniqueId;
    }

    private void UpdateAlphaCanvas(float value)
    {
        canvasGpDialog.alpha = value;
    }
}
