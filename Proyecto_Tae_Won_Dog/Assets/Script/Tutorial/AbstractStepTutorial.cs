﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class AbstractStepTutorial : MonoBehaviour
{
    [SerializeField]
    protected TouchGameplayHandler blockTouchHandler;
    [SerializeField]
    protected TutorialDialogHandler tutoDialog;
    [SerializeField]
    protected string[] idsTextBox;

    [SerializeField]
    protected float timeBlockTouchBeforeDialog = 1f;
    [SerializeField]
    protected HandIconHandler handIconHandler;


    protected int currentIdText;
    public abstract void StartStep(Action onComplete);
    protected abstract IEnumerator RuningStepRutine(Action onComplete);

    bool isWaitTouch;

    private void Awake()
    {
        if (blockTouchHandler != null)
            blockTouchHandler.gameObject.SetActive(false);
    }


    protected IEnumerator WaitTouchBlock(float timeTouchScreenClose = 0f)
    {
        if (blockTouchHandler == null)
            yield break;

        isWaitTouch = true;

        SettingConfigManager.Instance.SetInputMode(InputMode.UI);
        var callbackTrigger = false;
        blockTouchHandler.gameObject.SetActive(true);

        yield return null;

        var previousSelectedGameObject = EventSystem.current.currentSelectedGameObject;
        EventSystem.current.SetSelectedGameObject(blockTouchHandler.gameObject);

        void handle() { callbackTrigger = true; };
        blockTouchHandler.OnPointerDownEvent += handle;
        SettingConfigManager.Instance.OnUISubmit += handle;

        while (!callbackTrigger)
            yield return null;

        blockTouchHandler.OnPointerDownEvent -= handle;
        SettingConfigManager.Instance.OnUISubmit -= handle;

        if (timeTouchScreenClose > 0f)
            yield return new WaitForSecondsRealtime(timeTouchScreenClose);
        blockTouchHandler.gameObject.SetActive(false);
        EventSystem.current.SetSelectedGameObject(previousSelectedGameObject);

        isWaitTouch = false;
        SettingConfigManager.Instance.SetInputMode(InputMode.Gameplay);


    }
    protected void StartTempTouchBlock(float timeTouchScreenClose)
    {
        if (isWaitTouch)
        {
            Debug.LogWarning($"Skip temp touch because is active wait touch. GO: {gameObject.name}");
        }
        StartCoroutine(TempTouchBlock(timeTouchScreenClose));
    }

    protected IEnumerator TempTouchBlock(float timeTouchScreenClose)
    {
        if (blockTouchHandler == null)
            yield break;

        if (isWaitTouch)
        {
            Debug.LogWarning($"Skip temp touch because is active wait touch. GO: {gameObject.name}");
            yield break;
        }

        blockTouchHandler.gameObject.SetActive(true);
        yield return new WaitForSecondsRealtime(timeTouchScreenClose);
        blockTouchHandler.gameObject.SetActive(false);
    }

    public string GetNextText()
    {
        if (idsTextBox == null)
            return null;
        if (currentIdText >= idsTextBox.Length)
            return null;
        var idText = idsTextBox[currentIdText];
        currentIdText++;
        return idText;
    }

    protected virtual void OnDisable()
    {
        CorutinePingPong.ForceStop();
        StopAllCoroutines();
    }

#if UNITY_EDITOR

    private void OnValidate()
    {
        if (idsTextBox == null)
            return;

        if (LocalizationManager.Instance && idsTextBox.Length > 0)
        {
            for (int i = 0; i < idsTextBox.Length; i++)
            {
                var id = idsTextBox[i];
                if (!id.Contains('*'))
                {
                    continue;
                }

                id = id.Replace("*", "");
                string keyFound = LocalizationManager.Instance.TryGetNearLocaleKey(id);
                if (keyFound == null)
                {
                    Debug.LogWarning($"Not found matches locale key : [{id}]");
                    continue;
                }

                idsTextBox[i] = keyFound;
            }
        }
    }

#endif
}
