﻿using UnityEngine;
using System.Collections;

public class PosicionRamdom : MonoBehaviour {
	public int posicionMinima = 0;
	public int posicionMaxima = 1;
	// Use this for initialization
	void Start () {
		int valorRandom = Random.Range (posicionMinima, posicionMaxima);
		transform.position = new Vector2 (transform.position.x,transform.position.y + valorRandom);
	}
}
