﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextureScroll : MonoBehaviour
{
    public float velocidad = 0f;
    private RawImage imgScroll;

    private void OnEnable()
    {
        GlobaGameplaylEvents.OnStartRun += StartScrollingTexture;
        GlobaGameplaylEvents.OnGameOverShowCompleted += StopAllCoroutines;
    }

    private void OnDisable()
    {
        StopAllCoroutines();
        GlobaGameplaylEvents.OnStartRun -= StartScrollingTexture;
        GlobaGameplaylEvents.OnGameOverShowCompleted -= StopAllCoroutines;
    }

    private void StartScrollingTexture()
    {
        StartCoroutine(ScrollTextureRutine());
    }

    private IEnumerator ScrollTextureRutine()
    {
        while (true)
        {
            var mov = Time.deltaTime * velocidad;
            var rect = ImgScroll.uvRect;
            rect.x += mov;
            ImgScroll.uvRect = rect;
            yield return null;
        }
    }

    public RawImage ImgScroll {
        get
        {
            if (imgScroll == null)
                imgScroll = GetComponent<RawImage>();
            return imgScroll;
        }
    }
}
