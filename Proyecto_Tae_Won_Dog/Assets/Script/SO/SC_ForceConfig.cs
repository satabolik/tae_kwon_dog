﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RNG = UnityEngine.Random;
[CreateAssetMenu(fileName = "ForceConfig", menuName = "ScriptableObjects/Force Config", order = 0)]
public class SC_ForceConfig : ScriptableObject
{
    [SerializeField]
    private float forceMinX;
    [SerializeField]
    private float forceMaxX;
    [SerializeField]
    private float forceMinY;
    [SerializeField]
    private float forceMaxY;

    public float GetRngForceX()
    {
        var forceX = RNG.Range(forceMinX, forceMaxX);
        return forceX;
    }
    public float GetRngForceY()
    {
        var forceY = RNG.Range(forceMinY, forceMaxY);
        return forceY;
    }
    public float ForceMinX { get => forceMinX; }
    public float ForceMaxX { get => forceMaxX; }
    public float ForceMinY { get => forceMinY; }
    public float ForceMaxY { get => forceMaxY; }
}
