﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "New Localization Config", menuName = "ScriptableObjects/Localization Config", order = 0)]
public class SC_LocalizationConfig : ScriptableObject
{
    [SerializeField]
    private LocaleTypes localeType;
    [SerializeField]
    private LocalePair[] localeValues;

    public string GetLocValue(string key)
    {
        if (LocaleValues == null)
            return null;
        foreach (var pair in LocaleValues)
        {
            if (pair.key.Contains(key, System.StringComparison.OrdinalIgnoreCase))
                return pair.value;
        }
        return null;
    }
    public LocaleTypes LocaleType { get => localeType;}
    public LocalePair[] LocaleValues { get => localeValues; }

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (localeValues == null || localeValues.Length == 0)
            return;

        //localeValues = localeValues.OrderBy((pair)=> pair.key).ToArray();
    }
#endif
}
