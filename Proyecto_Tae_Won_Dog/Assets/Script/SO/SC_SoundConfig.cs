﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundConfig", menuName = "ScriptableObjects/Sound Config", order = 1)]
public class SC_SoundConfig : ScriptableObject
{
    [SerializeField]
    private SfxDat[] soundsFX;

    public SfxDat[] SoundsFX { get => soundsFX;}



    public SfxDat GetSoundDat(SfxType type)
    {
        if (soundsFX == null)
            return default;
        foreach (var sfx in SoundsFX)
        {
            if (sfx.type == type)
                return sfx;
        }
        return default;
    }

    private void OnValidate()
    {
        if (soundsFX == null)
            return;
        for (int i = 0; i < soundsFX.Length; i++)
        {
            var sfx = soundsFX[i];
            sfx.name = soundsFX[i].type.ToString();
            soundsFX[i] = sfx;
        }
    }

}

[System.Serializable]
public struct SfxDat
{
    [HideInInspector]
    public string name;
    public SfxType type;
    public AudioClip[] clips;
}

public enum SfxType
{
    NINGUNO = 0,
    HitPlank = 1,
    HitPlayer = 2,
    lifeItemPick = 3,
    playerAtack = 4,
    PressButton = 5
}