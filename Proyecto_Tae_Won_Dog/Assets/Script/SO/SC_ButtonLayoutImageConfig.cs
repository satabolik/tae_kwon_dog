using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using static UnityEngine.SpatialTracking.TrackedPoseDriver;

[CreateAssetMenu(fileName = "ButtonLayoutImageConfig", menuName = "ScriptableObjects/Button Layout Image Config", order = 0)]
public class SC_ButtonLayoutImageConfig : ScriptableObject
{
    [SerializeField]
    private ButtonLayout[] buttonLayouts;

    public Sprite[] GetButtonSprite(DeviceType deviceType, ButtonActionType actionType)
    {
        foreach (var layout in buttonLayouts) 
        {
            if (layout.GetSprite(deviceType,actionType, out Sprite[] sprites))
            {
                return sprites;
            }
        }

        return null;
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        for (int i = 0; i < buttonLayouts.Length; i++)
        {
            var layout = buttonLayouts[i];
            layout.UpdateDescription();
            buttonLayouts[i] = layout;
        }
    }
#endif
}

[System.Serializable]
public struct ButtonSpriteDevice
{
#if UNITY_EDITOR
    [HideInInspector]
    public string desc;
#endif
    [SerializeField]
    private DeviceType deviceType;
    [SerializeField]
    private Sprite[] buttonSprites;

    public DeviceType DeviceType { get => deviceType; }
    public Sprite[] ButtonSprites { get => buttonSprites; }

#if UNITY_EDITOR
    public void UpdateDescription()
    {
        string spritesNames = string.Join(", ", buttonSprites.Select(x => x.name).ToArray());
        desc = $"[{deviceType}]-> sprites: [{spritesNames}]";
    }
#endif
}


[System.Serializable]
public struct ButtonLayout
{
#if UNITY_EDITOR
    [HideInInspector]
    public string desc;
#endif
    [SerializeField]
    private ButtonSpriteDevice[] btnSpriteDevices;
    [SerializeField]
    private ButtonActionType[] buttonActionTypes;

    public ButtonSpriteDevice[] BtnSpriteDevices { get => btnSpriteDevices; }
    public ButtonActionType[] ButtonActionTypes { get => buttonActionTypes;}

    public bool GetSprite(DeviceType deviceType, ButtonActionType actionType, out Sprite[] outSprites)
    {
        outSprites = null;
        if (!buttonActionTypes.Contains(actionType))
        {
            return false;
        }

        foreach (var spriteDevice in btnSpriteDevices)
        {
            if (spriteDevice.DeviceType == deviceType)
            {
                outSprites = spriteDevice.ButtonSprites;
                return true;
            }
        }

        return false;
    }

#if UNITY_EDITOR
    public void UpdateDescription()
    {
        string devicesNames = string.Join(", ", btnSpriteDevices.Select(x => x.DeviceType).ToArray());
        string actionNames = string.Join(", ", buttonActionTypes);

        desc = $"[{devicesNames}] - [{actionNames}]";

        for (int i = 0; i < btnSpriteDevices.Length; i++)
        {
            var spriteDevice = btnSpriteDevices[i];
            spriteDevice.UpdateDescription();
            btnSpriteDevices[i] = spriteDevice;
        }
    }
#endif
}

public enum ButtonActionType
{
    Accept = 0,
    Back = 1,
    Continue = 2,
    Setting = 3,
    ActivePowerUp = 4,
    ScrollUpDown = 5,
    ActionGameplay = 6
}