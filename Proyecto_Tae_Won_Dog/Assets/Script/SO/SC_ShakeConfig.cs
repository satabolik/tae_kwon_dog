﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "ShakeConfig", menuName = "ScriptableObjects/Shake Config", order = 0)]
public class SC_ShakeConfig : ScriptableObject
{
    [SerializeField]
    private float magnitude;
    [SerializeField]
    private float roughness;
    [SerializeField]
    private float fadeInTime;
    [SerializeField]
    private float fadeOutTime;

    public float Magnitude { get => magnitude; }
    public float Roughness { get => roughness; }
    public float FadeInTime { get => fadeInTime;}
    public float FadeOutTime { get => fadeOutTime; }
}
