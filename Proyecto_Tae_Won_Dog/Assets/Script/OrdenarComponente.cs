﻿using UnityEngine;
using System.Collections;

public class OrdenarComponente : MonoBehaviour {
	public string nombreCapa = "";
	public int numeroDeOrden = 0;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer>().sortingLayerName = nombreCapa;
		GetComponent<Renderer>().sortingOrder = numeroDeOrden;
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
