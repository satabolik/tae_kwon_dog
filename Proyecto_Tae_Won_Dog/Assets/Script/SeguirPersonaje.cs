﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SeguirPersonaje : MonoBehaviour
{
    [SerializeField]
    private List<CameraSizePortrait> listCameSizePortraits;

    public Transform personaje;
    public float separacion = 0f;
    private ScreenOrientation screen;

    private float landscapeDefaultSize;
    private Camera cam;

    private const float BASE_ZOOM_CALCULATE_RATIO = 61.36363636363636f;

#if UNITY_ANDROID
    private void Awake()
    {
        cam = GetComponent<Camera>();
        landscapeDefaultSize = cam.orthographicSize;
    }


    // Update is called once per frame
    void Update()
    {
        if (personaje != null)
        {
            if (screen != GetScreenOrientation())
            {
                screen = GetScreenOrientation();
                UpdateSizeCamera();
#if UNITY_EDITOR
                var w = Screen.width;
                var h = Screen.height;
                Debug.Log($"mode : {screen}, Res: {w}x{h} : {(w / (float)h)} Cam Size: {cam.orthographicSize}");
#endif
            }

        }

    }


    private void UpdateSizeCamera()
    {
        if (screen == ScreenOrientation.Portrait ||
            screen == ScreenOrientation.PortraitUpsideDown)
        {
            var ratioStr = (Screen.width / (float)Screen.height).ToString("0.00");
            var ratio = float.Parse(ratioStr);
            float sizeSelect = BASE_ZOOM_CALCULATE_RATIO * (1f - ratio);
            cam.orthographicSize = sizeSelect;
        }
        else
        {
            cam.orthographicSize = landscapeDefaultSize;
        }
    }

    private ScreenOrientation GetScreenOrientation()
    {
#if UNITY_EDITOR
        if (Screen.height > Screen.width)
        {
            return ScreenOrientation.Portrait;
        }
        else
        {
            return ScreenOrientation.LandscapeLeft;
        }
#else
            return Screen.orientation;
#endif
    }
#endif
    [System.Serializable]
    public struct CameraSizePortrait
    {
        public float aspectValue;
        public float size;
    }
}
