﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FondoMovedizo : MonoBehaviour {
	public float velocidad = 0f;
	private RawImage imgPatron;
	// Use this for initialization
	void Start () {
		imgPatron = GetComponent<RawImage>();
	}


	// Update is called once per frame
	void Update () {
		var moveOffSet = (Time.time * velocidad) % 1;
		var uvRect = imgPatron.uvRect;
		uvRect.x = moveOffSet;
		uvRect.y = moveOffSet;
		imgPatron.uvRect = uvRect;
		//GetComponent<Renderer>().material.mainTextureOffset = new Vector2 ((Time.time * velocidad)%1,(Time.time * velocidad)%1);
	}
}
